package lab5;

import java.util.ArrayList;

public class Zadanie1 {
    public static void main(String[] args){

        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        ArrayList<Integer> b = new ArrayList<>();
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);

        System.out.println("a append b: " + append(a, b).toString());
    }

    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){

        ArrayList<Integer> c = new ArrayList<>();
        for (int i = 0; i < a.size(); i++){
            c.add(a.get(i));
        }
        for (int i = 0; i < b.size(); i++){
            c.add(b.get(i));
        }
        return c;
    }
}
