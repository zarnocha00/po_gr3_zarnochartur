package lab5;

import java.util.ArrayList;

public class Zadanie2 {
    public static void main(String[] args){

        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        ArrayList<Integer> b = new ArrayList<>();
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);

        System.out.println("a merge b: " + merge(a, b).toString());
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){

        ArrayList<Integer> c = new ArrayList<>();
        int zapis_z_petli = 0;

        if(a.size() < b.size()) {
            for (int i = 0; i < a.size(); i++) {
                c.add(a.get(i));
                c.add(b.get(i));
                zapis_z_petli = i;
            }

            zapis_z_petli++;
            for (int i = zapis_z_petli; i < b.size(); i++) {
                c.add(b.get(i));
            }
        }
        else{
            for (int i = 0; i < b.size(); i++) {
                c.add(a.get(i));
                c.add(b.get(i));
                zapis_z_petli = i;
            }

            zapis_z_petli++;
            for (int i = zapis_z_petli; i < a.size(); i++) {
                c.add(a.get(i));
            }
        }
        return c;
    }
}
