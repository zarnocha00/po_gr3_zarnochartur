package lab5;

import java.util.ArrayList;

public class Zadanie5 {
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        a.add(37);

        reverse(a);
        System.out.println("Odwrocony ArrayList: " + a);
    }

    public static void reverse(ArrayList<Integer> a) {
        for(int i = 0, j = a.size()-1; i < (a.size())/2; i++, j--) {
            int tmp = a.get(i);
            a.set(i, a.get(j));
            a.set(j, tmp);
        }
    }
}
