package lab5;

import java.util.ArrayList;

public class Zadanie3 {
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        ArrayList<Integer> b = new ArrayList<>();
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);

        System.out.println("a mergeSorted b: " + mergeSorted(a, b));
    }
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> c = new ArrayList<>();

        int tab_a_idx = 0;
        int tab_b_idx = 0;

        while(!((tab_b_idx == (b.size())) || (tab_a_idx == (a.size())))) {
            if (a.get(tab_a_idx) <= b.get(tab_b_idx)) {
                c.add(a.get(tab_a_idx));
                tab_a_idx++;
            } else {
                c.add(b.get(tab_b_idx));
                tab_b_idx++;
            }
        }

        if(tab_a_idx != a.size()) {
            while(tab_a_idx != a.size()) {
                c.add(a.get(tab_a_idx));
                tab_a_idx++;
            }
        }
        else if(tab_b_idx != b.size()) {
            while(tab_b_idx != b.size()) {
                c.add(b.get(tab_b_idx));
                tab_b_idx++;
            }
        }

        return c;
    }
}
