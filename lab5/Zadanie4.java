package lab5;

import java.util.ArrayList;

public class Zadanie4 {
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        System.out.println("Odwrocony ArrayList: " + reversed(a));
    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a) {
        ArrayList<Integer> result = new ArrayList<>();

        for(int i = a.size()-1; i >= 0; i--) {
            result.add(a.get(i));
        }

        return result;
    }
}
