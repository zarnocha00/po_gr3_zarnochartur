package lab13;

import java.util.PriorityQueue;
import java.util.Scanner;

class Zadanie implements java.lang.Comparable {
    private int id;
    private String opis;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Zadanie(String opis) {
        this.id = getId();
        this.opis = opis;
    }
    public Zadanie(String opis, int id) {
        this.id = id;
        this.opis = opis;
    }

    @Override
    public int compareTo(Object o) {
        // zmienilem znaki > <
        Zadanie zad = ((Zadanie) o);
//        o = (Zadanie) o;
        if (this.getId() < zad.getId()) {
            return  1;
        }
        else if (this.getId() > zad.getId()) {
            return -1;
        }
        else return 0;
    }
}

public class zad1 {
    public static void main(String[] args) {
        PriorityQueue<Zadanie> zadania = new PriorityQueue<>();
        String odpowiedz = " ";
        Scanner scan = new Scanner(System.in);

        System.out.println("Co chcesz zrobic?:");
        System.out.println("Dostępne polecenia:");
        System.out.println("-> 'dodaj priorytet opis'");
        System.out.println("-> 'następne'");
        System.out.println("-> 'zakończ'");

        while (!(odpowiedz.equalsIgnoreCase("zakończ"))) {
            System.out.println("==============\n");
            System.out.print("odp: ");
            odpowiedz = scan.nextLine();

            System.out.println("++++++++++++++");
            if(odpowiedz.contains("dodaj priorytet")) {
//                String zadanie_string = odpowiedz;
                String[] zadanie_array = odpowiedz.split(" ");
                int ilosc_zadan = zadania.size();
                Zadanie zadanie = new Zadanie(zadanie_array[2], ilosc_zadan);


                String opis_zadania = zadanie.getOpis();

                if (zadanie_array.length >= 3) {
                    for (int i = 3; i < zadanie_array.length; i++) {
//                        opis_zadania.join(" ", opis_zadania, zadanie_array[i]);
                        opis_zadania = opis_zadania.concat(" " + zadanie_array[i]);

                    }
                    zadania.add(zadanie); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    zadanie.setOpis(opis_zadania);
                }
                else {
                    System.out.println("Błąd");
                }
            }

            if (odpowiedz.equalsIgnoreCase("następne")) {
                if (zadania.size() > 0) {
                    System.out.println("\nOto zadanie:\n" + zadania.poll().getOpis() + "\n");
                }
                else {
                    System.out.println("\nBrak zadań!");
                }
            }

            if(odpowiedz.equalsIgnoreCase("zakoncz")) break;
            if(!(odpowiedz.equalsIgnoreCase("zakończ"))) System.out.println("Co chcesz zrobic?:");
        }
    }
}
