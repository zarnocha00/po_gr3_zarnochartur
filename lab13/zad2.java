package lab13;
import java.util.*;

public class zad2 {
    public static void main(String[] args) {
        TreeMap<String, String> dziennik = new TreeMap<>();
        String odpowiedz = "";
        Scanner scan = new Scanner(System.in);

        System.out.println("Co chcesz zrobic?:");
        System.out.println("\nDostępne polecenia:");
        System.out.println("-> 'wypisz'");
        System.out.println("-> 'dodaj'");
        System.out.println("-> 'usuń'");
        System.out.println("-> 'zmień'");
        System.out.println("-> 'zakończ'");

        while (!(odpowiedz.equalsIgnoreCase("zakończ"))) {

            System.out.println("==============\n");
            System.out.print("odpowiedź: ");
            odpowiedz = scan.nextLine();

            System.out.println("++++++++++++++");

            if (odpowiedz.equalsIgnoreCase("wypisz")) {

                if (dziennik.isEmpty()) {
                    System.out.println("Pusty dziennik!");
                }

                for (Map.Entry<String, String> wpis: dziennik.entrySet()) {

                    String imie = wpis.getKey();
                    String ocena = wpis.getValue();
                    System.out.println(imie + ": " + ocena);

                }
            }

            if (odpowiedz.equalsIgnoreCase("dodaj")) {

                System.out.println("Podaj imie: ");
                String imie = scan.nextLine();

                System.out.println("Podaj ocene:");
                String ocena = scan.nextLine();
                dziennik.put(imie, ocena);

                System.out.println("\nDodano studenta " + imie + "!\n");

            }

            if (odpowiedz.equalsIgnoreCase("usuń")) {

                System.out.println("Podaj imie studenta, ktorego chcesz usunac:");
                String imie = scan.nextLine();
                dziennik.remove(imie);

                System.out.println("\nUsunieto studenta " + imie + "!\n");

            }

            if (odpowiedz.equalsIgnoreCase("zmień")) {

                System.out.println("Podaj imie studenta, ktorego ocene chcesz zmienic:");
                String imie = scan.nextLine();
                scan.reset();
                System.out.println("Podaj nowa ocene:");
                String ocena = scan.nextLine();

                dziennik.replace(imie, ocena);

                System.out.println("Zmieniono ocene studenta " + imie + "!");

            }

        }
    }
}
