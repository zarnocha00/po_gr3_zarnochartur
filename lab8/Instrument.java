package lab8;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

abstract class Instrument {
    String producent;
    LocalDate rokProdukcji;

    Instrument(String producent, LocalDate rokProdukcji) {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    Instrument() {

        Scanner scan = new Scanner(System.in);

        System.out.println("Podaj producenta: ");
        this.producent = scan.nextLine();

        System.out.println("Podaj rok produkcji: ");
        this.rokProdukcji = dodajDate(scan);

    }

    public String getProducent() {
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    abstract String dzwiek();

    public LocalDate dodajDate(Scanner scan) {

        System.out.print("Wprowadz date w formacie dzien.miesiac.rok: ");
        String data = scan.next();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        return LocalDate.parse(data, dtf);
    }

    public boolean equals(Instrument o) {
        if (this.rokProdukcji.isEqual(o.rokProdukcji)) {
            return this.producent.equalsIgnoreCase(o.producent);
        }
        return false;
    }

    @Override
    public String toString() {
        return "producent - " + getProducent() + ", rok produkcji: " + getRokProdukcji();
    }

}