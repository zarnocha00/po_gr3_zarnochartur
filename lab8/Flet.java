package lab8;

import java.time.LocalDate;

public class Flet extends Instrument {

    public Flet() {
        super();
    }

    public Flet(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    String dzwiek() {
        return "fiu fiu fiu";
    }

    @Override
    public String toString() {
        return "Flet: " + super.toString();
    }

}
