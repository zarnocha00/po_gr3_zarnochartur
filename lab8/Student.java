
package lab8;

import java.time.LocalDate;
import java.util.Scanner;

public class Student extends lab8.Osoba {

    private String kierunek;
    private double sredniaOcen;

    public Student() {

        super();

        System.out.print("Podaj kierunek: ");
        Scanner scanner = new Scanner(System.in);
        setKierunek(scanner.nextLine());

        System.out.print("Podaj srednia ocen (z przecinkiem): ");
        setSredniaOcen(scanner.nextDouble());

    }

    public Student(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen) {

        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;

    }

    public void setKierunek(String kierunek) {
        this.kierunek = kierunek;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    public String getKierunek() {
        return kierunek;
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    @Override
    public String toString() {
        return super.toString() + ", kierunek studiów: " + getKierunek() + ", srednia ocen: " + getSredniaOcen();
    }

}