package lab8;

import java.time.LocalDate;

public class Skrzypce extends Instrument {

    public Skrzypce() {
        super();
    }

    public Skrzypce(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    String dzwiek() {
        return "łe łe łe";
    }

    @Override
    public String toString() {
        return "Skrzypce: " + super.toString();
    }

}
