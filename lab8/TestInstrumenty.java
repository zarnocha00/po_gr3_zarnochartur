package lab8;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args) {

        ArrayList<Instrument> orkiestra = new ArrayList<>();

        Flet flet1 = new Flet("Yamaha", LocalDate.parse("2000-08-21"));
        Fortepian fortepian1 = new Fortepian("Solemio", LocalDate.parse("2002-01-03"));
        Skrzypce skrzypce1 = new Skrzypce("Collo", LocalDate.parse("2005-06-22"));
        Flet flet2 = new Flet("Yamaha", LocalDate.parse("2012-12-07"));
        Fortepian fortepian2 = new Fortepian("Musican", LocalDate.parse("2021-12-06"));

        Fortepian fortepian3 = new Fortepian("Musican", LocalDate.parse("2021-12-02"));
        Fortepian fortepian4 = new Fortepian("Musican", LocalDate.parse("2021-12-02"));

        orkiestra.add(flet1);
        orkiestra.add(fortepian1);
        orkiestra.add(skrzypce1);
        orkiestra.add(flet2);
        orkiestra.add(fortepian2);

        System.out.println("Fonetyczny wystep orkiestry:");
        for (Instrument instrument : orkiestra) {
                System.out.println(instrument.dzwiek());
        }

        System.out.println("\nOrkiestra: " + orkiestra);

        System.out.println("\nfortepian2.equals(fortepian3): " + fortepian2.equals(fortepian3));
        System.out.println("\nfortepian3.equals(fortepian4): " + fortepian3.equals(fortepian4));

    }

}
