package lab8;

import java.time.LocalDate;

public class Fortepian extends Instrument {

    public Fortepian() {
        super();
    }

    public Fortepian(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    String dzwiek() {
        return "dum dum dum";
    }

    @Override
    public String toString() {
        return "Fortepian: " + super.toString();
    }

}