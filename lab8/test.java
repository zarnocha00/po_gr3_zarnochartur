package lab8;
import java.time.LocalDate;

public class test {
    public static void main(String[] args) {

        Osoba osobnik = new Osoba();
        System.out.println(osobnik);

        Osoba osobnik2 = new Osoba("Osoba", new String[]{"Osobnik2", "2kinbosO"}, LocalDate.parse("2000-01-01"), true);
        System.out.println(osobnik2 + "\n");

        Pracownik pracek = new Pracownik();
        System.out.println(pracek);

        Pracownik pracus = new Pracownik("Pracownik", new String[]{"pracus", "sucarp"}, LocalDate.parse("1999-09-09"), false, 5400.99f, LocalDate.parse("2008-09-01"));
        System.out.println(pracus + "\n");

        Student studencicho = new Student();
        System.out.println(studencicho);

        Student stud = new Student("Student", new String[]{"stud", "duts"}, LocalDate.parse("1999-09-09"), true, "informatyka", 4.5);
        System.out.println(stud);

    }
}
