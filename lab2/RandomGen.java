package lab2;

public class RandomGen {
     public int[] rand(int n){
         
         int min = -999;
         int max = 999;
         
         int[] tab = new int[n];
         
         for(int i = 0; i < n; i++)
         {
            tab[i] = (int)(Math.random() * ((max - min) + 1)) + min;
         }
         
         return tab;        
     }
     public void wyswietl(int[] tab, int n){
         for(int i = 0; i < n-1; i++)
         {
             System.out.print(tab[i] + ", ");
         }
         System.out.print(tab[n-1] + "\n");
     }
}
