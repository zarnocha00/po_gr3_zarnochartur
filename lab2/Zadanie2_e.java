package lab2;
import java.util.Random;
import java.util.Scanner;

public class Zadanie2_e {
    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = scan.nextInt();
        
        while(n < 1 || n > 100)
        {
            System.out.print("Bledna wartosc. Sprobuj ponownie: ");
            n = scan.nextInt();
        }

        int[] tab = new int[n];

        generuj(tab, n, -999, 999);
        wypisz(tab);
        
        System.out.println("Dlugosc maksymalnego ciagu dodatnich wynosi: " + dlugoscMaksymalnegoCiaguDodatnich(tab));
    }
    
    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        maxWartosc *= 2;
        for(int i = 0; i < n; i++)
        {
            tab[i] = (r.nextInt(maxWartosc+1)) + minWartosc;
        }
    }
    
    public static void wypisz(int[] tab) {
        for (int el : tab) {
            System.out.print(el + " ");
        }
        System.out.println("");
    }
    
    public static int dlugoscMaksymalnegoCiaguDodatnich (int tab[]) {
        int dlugosc_biezaca = 0;
        int dlugosc = 0;
    
        for(int i = 0; i < tab.length; i++)
        {
            if(tab[i] > 0)
            {
                dlugosc_biezaca++;
            }
            else
            {
                if(dlugosc < dlugosc_biezaca)
                {
                    dlugosc = dlugosc_biezaca;
                }
                dlugosc_biezaca = 0;
            }   
        }
        if(dlugosc < dlugosc_biezaca)   
        {
            dlugosc = dlugosc_biezaca;
        }
        
        return dlugosc;
    }
}
