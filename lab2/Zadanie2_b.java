package lab2;
import java.util.Random;
import java.util.Scanner;

public class Zadanie2_b {
    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = scan.nextInt();
        
        while(n < 1 || n > 100)
        {
            System.out.print("Bledna wartosc. Sprobuj ponownie: ");
            n = scan.nextInt();
        }

        int[] tab = new int[n];

        generuj(tab, n, -999, 999);
        wypisz(tab);
        
        System.out.println("\nLiczb dodatnich: " + ileDodatnich(tab) + "\nZer: " + ileZerowych(tab) + "\nLiczb ujemnych: " + ileUjemnych(tab));

    }
    
    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        maxWartosc *= 2;
        for(int i = 0; i < n; i++)
        {
            tab[i] = (r.nextInt(maxWartosc+1)) + minWartosc;
        }
    }
    
    public static void wypisz(int[] tab) {
        for (int el : tab) {
            System.out.print(el + " ");
        }
        System.out.println("");
    }
    
    public static int ileDodatnich (int tab[]){
        int ilosc_dodatnich = 0;
        for(int i = 0; i < tab.length; i++)
        {
            if(tab[i] > 0)
            {
                ilosc_dodatnich++;
            }
        }
        return ilosc_dodatnich;
    }
                
    public static int ileUjemnych (int tab[]){
        int ilosc_ujemnych = 0;
        for(int i = 0; i < tab.length; i++)
        {
            if(tab[i] < 0)
            {
                ilosc_ujemnych++;
            }
        }
        return ilosc_ujemnych;
    }
                
    public static int ileZerowych (int tab[]){
        int ilosc_zer = 0;
        for(int i = 0; i < tab.length; i++)
        {
            if(tab[i] == 0)
            {
                ilosc_zer++;
            }
        }
        return ilosc_zer;
    }
                
    
}