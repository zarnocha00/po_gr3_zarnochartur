package lab2;
import java.util.Random;
import java.util.Scanner;

/*
Napisz program, który wczyta trzy liczby całkowite m, n, k (każda z przedziału [1; 10]), po
czym wygeneruje dwie macierze: macierz a o rozmiarach m×n oraz macierz b o rozmiarach
n×k. Następnie wypisze obie macierze na ekran, obliczy iloczyn macierzy a i b w macierzy
c oraz wypisze macierz c na ekran.
*/

public class Zadanie3 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        System.out.print("Podaj m: ");
        int m = s.nextInt();
        while(m < 1 || m > 10)
        {
            System.out.print("Bledna wartosc. Sprobuj ponownie: ");
            m = s.nextInt();
        }
        
        System.out.print("Podaj n: ");
        int n = s.nextInt();
        while(n < 1 || n > 10)
        {
            System.out.print("Bledna wartosc. Sprobuj ponownie: ");
            n = s.nextInt();
        }
        
        System.out.print("Podaj k: ");
        int k = s.nextInt();
        while(k < 1 || k > 10)
        {
            System.out.print("Bledna wartosc. Sprobuj ponownie: ");
            k = s.nextInt();
        }
        
        int[][] macierzA = new int[m][n];
        int[][] macierzB = new int[n][k];
        int[][] macierzC = new int[m][k];
        
        //generowanieMacierzy(macierz, minWartosc, maxWartosc);
        generowanieMacierzy(macierzA, 1, 9);
        generowanieMacierzy(macierzB, 1, 9);
        
        System.out.println("\nMacierz A: ");
        wypisz(macierzA);
        
        System.out.println("Macierz B: ");
        wypisz(macierzB);
        
        System.out.println("Wynik mnozenia w macierzy C: ");
        mnozenieMacierzy(macierzA, macierzB, macierzC, m, n, k);
        wypisz(macierzC);
    }
    
    
    public static void generowanieMacierzy(int tab[][], int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        //r.setSeed(1);
        if(minWartosc < 0)
        {
            maxWartosc *= 2;
            for(int i = 0; i < tab.length; i++)
            {
                for (int j = 0; j < tab[i].length; j++)
                {
                    tab[i][j] = (r.nextInt(maxWartosc+1)) + minWartosc;
                }
            }
        }
        else
        {
            for(int i = 0; i < tab.length; i++)
            {
                for (int j = 0; j < tab[i].length; j++)
                {
                    tab[i][j] = (r.nextInt(maxWartosc)) + minWartosc;
                }
            }
        } 
    }
    
    public static void wypisz(int[][] tab)
    {
        for (int[] innerArray: tab) {
            for(int data: innerArray) {
                System.out.printf("%3d ", data);
            }
            System.out.println("");
            }
        System.out.println("");
    }
    
    static void mnozenieMacierzy(int[][] A, int[][] B, int[][] C, int m, int n, int k)
    {
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < k; j++)
            {
                for (int l = 0; l < n; l++)
                {
                    C[i][j] += A[i][l] * B[l][j];
                }
            }
        }
    }
}