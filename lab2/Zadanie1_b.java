package lab2;

import java.util.Scanner;

public class Zadanie1_b {
    public static void main(String[] args){
    RandomGen randomClass = new RandomGen();
    Scanner scan = new Scanner(System.in);
    
    System.out.print("Podaj n: ");
    int n = scan.nextInt();
    while(n < 1 || n > 100)
    {
        System.out.print("Bledna wartosc. Sprobuj ponownie: ");
        n = scan.nextInt();
    }
    
    int tab[] = randomClass.rand(n);
    
    randomClass.wyswietl(tab, n);
    
    int ilosc_dodatnich = 0;
    int ilosc_ujemnych = 0;
    int ilosc_zer = 0;
    
    for(int i = 0; i < n; i++)
    {
        if(tab[i] > 0)
        {
            ilosc_dodatnich++;
        }
        else if(tab[i] < 0)
        {
            ilosc_ujemnych++;
        }
        else
        {
            ilosc_zer++;
        }
    }
    
    System.out.println("Liczb dodatnich: " + ilosc_dodatnich + "\nZer: " + ilosc_zer + "\nLiczb ujemnych: " + ilosc_ujemnych);
    
    }
}
