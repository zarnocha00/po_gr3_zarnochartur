package lab2;
import java.util.Scanner;


public class Zadanie1_a {
    public static void main(String[] args){
    lab2.RandomGen randomClass = new lab2.RandomGen();
    Scanner scan = new Scanner(System.in);
    
    
    System.out.print("Podaj n: ");
    int n = scan.nextInt();
    while(n < 1 || n > 100)
    {
        System.out.print("Bledna wartosc. Sprobuj ponownie: ");
        n = scan.nextInt();
    }
    
    int tab[] = randomClass.rand(n);
    
    randomClass.wyswietl(tab, n);
    
    int ilosc_nieparzystych = 0;
    int ilosc_parzystych = 0;
    
    for(int i = 0; i < n; i++)
    {
        if(tab[i] % 2 == 0)
        {
            ilosc_parzystych++;
        }
        else if(tab[i] % 2 != 0)
        {
            ilosc_nieparzystych++;
        }
    }
    
    System.out.println("Liczb parzystych: " + ilosc_parzystych + "\nLiczb nieparzystych: " + ilosc_nieparzystych);
    
    }
}
