package lab2;

import java.util.Scanner;

public class Zadanie1_g {
    public static void main(String[] args){
        
    RandomGen randomClass = new RandomGen();
    Scanner scan = new Scanner(System.in);
    
    
    System.out.print("Podaj n: ");
    int n = scan.nextInt();
    while(n < 1 || n > 100)
    {
        System.out.print("Bledna wartosc. Sprobuj ponownie: ");
        n = scan.nextInt();
    }
    
    //int tab[] = randomClass.rand(n);
    int tab[] = new int[n];
    for(int i=0; i < n; i++)
    {
        tab[i] = i+1;
    }
    
    System.out.print("\nTablica przed zamiana: ");
    randomClass.wyswietl(tab, n);
    
    System.out.print("\nPodaj lewy (1 <= lewy < n): ");
    int lewy = scan.nextInt();
    while(lewy < 1 || lewy >= n)
    {
        System.out.print("Bledna wartosc. Sprobuj ponownie: ");
        lewy = scan.nextInt();
    }
    lewy--;
    
    System.out.print("Podaj prawy (1 <= prawy < n): ");
    int prawy = scan.nextInt();
    while(prawy < 1 || prawy > n || prawy < lewy)
    {
        System.out.print("Bledna wartosc. Sprobuj ponownie: ");
        prawy = scan.nextInt();
    }
    prawy--;
    
    int srednia = (int)Math.floor((prawy-lewy)/2);
    //System.out.println(srednia);
    for(int l = lewy, p = prawy; l <= lewy+srednia; l++, p--)
    {
        int tmp = tab[l];
        tab[l] = tab[p];
        tab[p] = tmp;      
    }
    
    System.out.print("\nTablica po zamianie: ");
    randomClass.wyswietl(tab, n);
    System.out.print("\n");
    
    // tablicę indeksuję zgodnie z poleceniem od 1.
    }       
}
