package lab2;

import java.util.Scanner;

public class Zadanie1_f {
    public static void main(String[] args){
        
    RandomGen randomClass = new RandomGen();
    Scanner scan = new Scanner(System.in);
    
    
    System.out.print("Podaj n: ");
    int n = scan.nextInt();
    while(n < 1 || n > 100)
    {
        System.out.print("Bledna wartosc. Sprobuj ponownie: ");
        n = scan.nextInt();
    }
    
    int tab[] = randomClass.rand(n);
    
    System.out.print("\nTablica przed zamiana: ");
    randomClass.wyswietl(tab, n);
    

    for(int i = 0; i < n; i++)
    {
        if(tab[i] > 0)
        {
            tab[i] = 1;
        }
        else if(tab[i] < 0)
        {
            tab[i] = -1;
        }
    }
    
    System.out.print("Tablica po zamianie: ");
    randomClass.wyswietl(tab, n);
    }       
}
