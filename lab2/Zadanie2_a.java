package lab2;
import java.util.Random;
import java.util.Scanner;

public class Zadanie2_a {
    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);
    
    
        System.out.print("Podaj n: ");
        int n = scan.nextInt();
        
        while(n < 1 || n > 100)
        {
            System.out.print("Bledna wartosc. Sprobuj ponownie: ");
            n = scan.nextInt();
        }

        int[] tab = new int[n];

        generuj(tab, n, -999, 999);
        wypisz(tab);
        
        int parzyste = ileParzystych(tab);
        int nieparzyste = ileNieparzystych(tab);
        
        System.out.println("\nIlosc\nparzystych: " + parzyste + "\nnieparzystych: " + nieparzyste);
    }
    
    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        maxWartosc *= 2;
        for(int i = 0; i < n; i++)
        {
            tab[i] = (r.nextInt(maxWartosc+1)) + minWartosc;
        }
    }
    
    public static void wypisz(int[] tab) {
        for (int el : tab) {
            System.out.print(el + " ");
        }
        System.out.println("");
    }
    
    public static int ileNieparzystych (int tab[]){
        int ilosc_nieparzystych = 0;
        for(int i = 0; i < tab.length; i++)
        {
            if(tab[i] % 2 != 0)
            {
                ilosc_nieparzystych++;
            }
        }
        return ilosc_nieparzystych;
     }
    
    public static int ileParzystych (int tab[]){
        int ilosc_parzystych = 0;
        for(int i = 0; i < tab.length; i++)
        {
            if(tab[i] % 2 == 0)
            {
                ilosc_parzystych++;
            }
        }
         return ilosc_parzystych;
        }
}