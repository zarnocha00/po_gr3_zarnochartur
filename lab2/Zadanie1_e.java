package lab2;

import java.util.Scanner;

public class Zadanie1_e {
    public static void main(String[] args){
    RandomGen randomClass = new RandomGen();
    Scanner scan = new Scanner(System.in);
    
    
    System.out.print("Podaj n: ");
    int n = scan.nextInt();
    while(n < 1 || n > 100)
    {
        System.out.print("Bledna wartosc. Sprobuj ponownie: ");
        n = scan.nextInt();
    }
    
    int tab[] = randomClass.rand(n);
    randomClass.wyswietl(tab, n);
    
    int dlugosc_biezaca = 0;
    int dlugosc = 0;
    
    for(int i = 0; i < n; i++)
    {
        if(tab[i] > 0)
        {
            dlugosc_biezaca++;
        }
        else
        {
            if(dlugosc < dlugosc_biezaca)
            {
                dlugosc = dlugosc_biezaca;
            }
            dlugosc_biezaca = 0;
        }   
    }
    if(dlugosc < dlugosc_biezaca)   
    {
        dlugosc = dlugosc_biezaca;
    }
    
    System.out.println("Dlugosc najdluszego fragmentu tablicy gdzie elementy sa dodatnie: " + dlugosc);
    
    }
}
