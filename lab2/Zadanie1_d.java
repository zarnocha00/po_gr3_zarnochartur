package lab2;

import java.util.Scanner;

public class Zadanie1_d {
    public static void main(String[] args){
    RandomGen randomClass = new RandomGen();
    Scanner scan = new Scanner(System.in);
    
    
    System.out.print("Podaj n: ");
    int n = scan.nextInt();
    while(n < 1 || n > 100)
    {
        System.out.print("Bledna wartosc. Sprobuj ponownie: ");
        n = scan.nextInt();
    }
    
    int tab[] = randomClass.rand(n);
    
    randomClass.wyswietl(tab, n);
    
    int suma_ujemnych = 0;
    int suma_dodatnich = 0;
    
    for(int i = 0; i < n; i++)
    {
        if(tab[i] > 0)
        {
            suma_dodatnich += tab[i];
        }
        else if(tab[i] < 0)
        {
            suma_ujemnych += tab[i];
        }
    }
    
    System.out.println("Suma dodatnich: " + suma_dodatnich + "\nSuma ujemnych: " + suma_ujemnych);    
    }
}
