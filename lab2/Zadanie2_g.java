package lab2;
import java.util.Random;
import java.util.Scanner;

public class Zadanie2_g {
    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = scan.nextInt();
        
        while(n < 1 || n > 100)
        {
            System.out.print("Bledna wartosc. Sprobuj ponownie: ");
            n = scan.nextInt();
        }

        int[] tab = new int[n];

        generuj(tab, n, -999, 999);
        System.out.println("\nTablica przed zamiana: ");
        wypisz(tab);
        
        System.out.print("\nPodaj lewy (1 <= lewy <= n): ");
        int lewy = scan.nextInt();
        while(lewy < 1 || lewy >= n)
        {   
            System.out.print("Bledna wartosc. Sprobuj ponownie: ");
            lewy = scan.nextInt()   ;
        }
        lewy--;
        
        System.out.print("Podaj prawy (1 <= lewy <= prawy <= n): ");
        int prawy = scan.nextInt();
        while(prawy < 1 || prawy > n || prawy < lewy)
        {
            System.out.print("Bledna wartosc. Sprobuj ponownie: ");
            prawy = scan.nextInt();
        }
        prawy--;
        
        odwrocFragment(tab, lewy, prawy);
        
        System.out.println("\nTablica po zamianie: ");
        wypisz(tab);
    }
    
    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        maxWartosc *= 2;
        for(int i = 0; i < n; i++)
        {
            tab[i] = (r.nextInt(maxWartosc+1)) + minWartosc;
        }
    }
    
    public static void wypisz(int[] tab) {
        for (int el : tab) {
            System.out.print(el + " ");
        }
        System.out.println("");
    }
    
    public static void odwrocFragment(int tab[], int lewy, int prawy)
    {
        int srednia = (int)Math.floor((prawy-lewy)/2);
        for(int l = lewy, p = prawy; l <= lewy+srednia; l++, p--)
        {
            int tmp = tab[l];
            tab[l] = tab[p];
            tab[p] = tmp;      
        }
    }
}
