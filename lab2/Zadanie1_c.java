package lab2;

import java.util.Scanner;

public class Zadanie1_c {
    public static void main(String[] args){
    RandomGen randomClass = new RandomGen();
    Scanner scan = new Scanner(System.in);
    
    
    System.out.print("Podaj n: ");
    int n = scan.nextInt();
    while(n < 1 || n > 100)
    {
        System.out.print("Bledna wartosc. Sprobuj ponownie: ");
        n = scan.nextInt();
    }
    
    int tab[] = randomClass.rand(n);
    
    randomClass.wyswietl(tab, n);
    
    int najwiekszy_element = tab[0];
    for(int i = 1; i < n; i++)
    {
        if(najwiekszy_element < tab[i])
        {
            najwiekszy_element = tab[i];
        }
    }
    
    int ilosc_wystapien = 0;
    for(int i = 0; i < n; i++)
    {
        if(tab[i] == najwiekszy_element)
        {
            ilosc_wystapien++;
        }
    }
    
    if(ilosc_wystapien == 1)
    {
        System.out.println("Najwiekszy element tablicy to: " + najwiekszy_element + " i wystapil on " + ilosc_wystapien + " raz");
    }
    else System.out.println("Najwiekszy element tablicy to: " + najwiekszy_element + " i wystapil on " + ilosc_wystapien + " razy");
    
    }
}
