package lab2;
import java.util.Random;
import java.util.Scanner;


public class Zadanie2_c {
    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = scan.nextInt();
        
        while(n < 1 || n > 100)
        {
            System.out.print("Bledna wartosc. Sprobuj ponownie: ");
            n = scan.nextInt();
        }

        int[] tab = new int[n];

        generuj(tab, n, -999, 999);
        wypisz(tab);
        
        int ilosc_wystapien = ileMaksymalnych(tab);
        
        if(ilosc_wystapien == 1)
        {
            System.out.println("wystapil on " + ilosc_wystapien + " raz.");
        }
        else System.out.println("wystapil on " + ilosc_wystapien + " razy.");
    }
    
    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        maxWartosc *= 2;
        for(int i = 0; i < n; i++)
        {
            tab[i] = (r.nextInt(maxWartosc+1)) + minWartosc;
        }
    }
    
    public static void wypisz(int[] tab) {
        for (int el : tab) {
            System.out.print(el + " ");
        }
        System.out.println("");
    }
    
    public static int ileMaksymalnych (int tab[])
    {
        int najwiekszy_element = tab[0];
        for(int i = 1; i < tab.length; i++)
        {
            if(najwiekszy_element < tab[i])
            {
                najwiekszy_element = tab[i];
            }
        }
        // dodatkowo:
        System.out.println("\nNajwiekszy element: " + najwiekszy_element + ",");
        
        int ilosc_wystapien = 0;
        for(int i = 0; i < tab.length; i++)
        {
            if(tab[i] == najwiekszy_element)
            {
                ilosc_wystapien++;
            }
        }
        
        return ilosc_wystapien;
    }
}
