package lab4;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Zadanie5 {
    public static void main(String[] args) {
        if(args.length < 3) {
            System.out.print("Przekazano za malo argumentow!");
            System.exit(-1);
        }

        BigDecimal k = BigDecimal.valueOf(Float.parseFloat(args[0])); // kapitał początkowy
        BigDecimal p = BigDecimal.valueOf(Float.parseFloat(args[1])); // stopa procentowa
        p = p.setScale(2, RoundingMode.CEILING);

        int n = Integer.parseInt(args[2]); // ilość lat
//       BigDecimal n = BigDecimal.valueOf(Float.parseFloat(args[2])); // ilość lat

        for(int i = 1; i < n; i++) {
            k = k.multiply(p);
            k = k.setScale(2, RoundingMode.CEILING);
        }
        System.out.print("Wielkosc kapitalu " + "z odsetkami " + p + " po " + n + " latach wynosi: " + k);
    }
}
