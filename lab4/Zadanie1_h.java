package lab4;

import java.util.Scanner;

public class Zadanie1_h {
    public static void main(String[] args){
        System.out.print("Podaj str: ");
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();

        char[] str_test = str.toCharArray();
        boolean poprawnosc = true;

        for (char c : str_test) {
            if (c > 57 || c < 48) {
                poprawnosc = false;
                break;
            }
        }

        while(!poprawnosc) {
            for(int i = 0; i < str_test.length; i++) {

                if (str_test[i] > 57 || str_test[i] < 48) {
                    poprawnosc = false;
                    System.out.print("Błąd przy wprowadzaniu str, sprobuj ponownie: ");
                    str = scan.nextLine();
                    str_test = str.toCharArray();
                    i = -1;     // zauwazylem, ze jezeli ustawilem i = 0, to petla (obserwujac w debuggerze) zaczynala iterowac od i = 1, wiec zmienilem na i = -1. Widac, ze petla robi ++i jezeli ponownie iteruje, bo teoretycznie nie istnieje indeks -1.
                }
                else {
                    poprawnosc = true;
                }
            }
        }

        System.out.print("Odstep separatora: ");
        int co_ile = scan.nextInt();

        System.out.print("Separator: ");
        char separator = scan.next().charAt(0);

        str = nice(str, co_ile, separator);
        System.out.print(str + "\n");

    }

    static String nice(String str, int co_ile, char separator){
        StringBuffer strbuf = new StringBuffer();
        strbuf.append(str);

        System.out.print("\n");
        int iter = 1;
        for(int i = str.length()-1; i >= 0; i--){

            if(iter % co_ile == 0 && iter != str.length()) {
                strbuf.insert(i, separator);
            }

            iter++;
        }

        return strbuf.toString();
    }

}
