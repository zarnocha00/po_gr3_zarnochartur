package lab4;
import java.util.Scanner;

public class Zadanie1_e {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj napis nr1: ");
        String str = scan.nextLine();
//        String str = "Jakies dluzsze zdanie";
        System.out.print("Podaj napis nr2: ");
        String subStr = scan.nextLine();
//        String subStr = "ie";
        
        int[] tablica_indeksow = where(str, subStr);
        System.out.println("\nTablica indeksow:");
        wyswietl(tablica_indeksow);
        
    }

    static void wyswietl(int[] tab)
    {
        for(int i = 0; i < tab.length; i++)
        {
            System.out.print(tab[i] + " ");
        }
        System.out.println("");
    }
    
    static int[] where(String str, String subStr)
    {
        int[] tablica = new int[str.length()]; //tablica do zwracania
        if(str.contains(subStr) == false)
        {
            return tablica;
        }

        int x = str.indexOf(subStr);
        for(int i = 0; i < str.length(); i=+x)
        {
            int j = str.indexOf(subStr, i);
            if(j != -1)
            {
                for(int k = str.indexOf(subStr, i); k <= (subStr.length()+j-1); k++)
                {
                    tablica[k] = 1;
                }
            }
            x = j+1;
            if(str.indexOf(subStr, i) == -1)
            {
                return tablica;
            }
        }
        return tablica;
    }
}
