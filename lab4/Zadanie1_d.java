package lab4;
import java.util.Scanner;

public class Zadanie1_d {
        public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj napis: ");
        String str = scan.nextLine();
        while(str.isEmpty())
        {
            System.out.print("Blad przy wprowadzaniu napisu, sprobuj ponownie: ");
            str = scan.nextLine();
        }
        System.out.print("Podaj n: ");
        int n = scan.nextInt();
        while(n < 0)
        {
            System.out.print("Blad przy wprowadzaniu liczby, sprobuj ponownie: ");
            n = scan.nextInt();
        }
        System.out.println(n + " * " + str + " = " + repeat(str, n));
    }
    static String repeat(String str, int n)
    {
        String concat = "";
        for(int i = 0; i < n; i++)
        {
            concat += str;
        }
        return concat;
    }
}
