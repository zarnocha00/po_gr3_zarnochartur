package lab4;

public class Zadanie2 {
    public static void main(String[] args) {

        if(args.length < 2) {
            System.out.print("Przekazano za malo argumentow!");
            System.exit(-1);
        }

        int ilosc_wystapien = 0;

        if (args[0].contains(args[1])) {
        for(int i = 0; i < args[0].length(); i++) {
                if(args[0].charAt(i) == args[1].charAt(0)) {
                    ilosc_wystapien++;
                }
            }
        }

        System.out.println("Ilosc wystapien \"" + args[1].charAt(0) + "\" w " + "\"" + args[0] + "\"" + " = " + ilosc_wystapien);
    }
}
