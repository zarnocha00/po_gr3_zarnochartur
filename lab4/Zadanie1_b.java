package lab4;
import java.util.Scanner;

public class Zadanie1_b {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj zdanie nr 1: ");
        String zdanie1 = scan.nextLine();
        System.out.print("Podaj zdanie nr 2: ");
        String zdanie2 = scan.nextLine();
        
        System.out.println("Ilosc wystapien: " + countSubStr(zdanie1, zdanie2));
    }
    public static int countSubStr(String str, String subStr)
    {
        int ilosc_wystapien = 0;
         
        if(str == null || str.length() == 0 || subStr == null || subStr.length() == 0)
        {
            return ilosc_wystapien;
        }
        
        int index = 0;
        while (true)
        {
            index = str.indexOf(subStr, index);
            if (index != -1) // zwraca -1 jezeli nie ma takiego subStringa w stringu
            {
                ilosc_wystapien++;
                index += subStr.length();
            }
            else
            {
                break;
            }
        }
 
        return ilosc_wystapien;
    }
}
