package lab4;
import java.util.Scanner;

public class Zadanie1_f {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj str: ");
        String str = scan.nextLine();

        System.out.print("\nZmieniony str: " + change(str));
    }

    static String change(String str)
    {
        StringBuffer strbuff = new StringBuffer();
        strbuff.append(str);
        for(int i = 0; i < strbuff.length(); i++)
        {
            if(strbuff.charAt(i) > 64 && strbuff.charAt(i) < 91) //duze litery na male
            {
                strbuff.setCharAt(i, (char)(strbuff.charAt(i)+32));
            }
            else if (strbuff.charAt(i) > 96 && strbuff.charAt(i) < 123) //male litery na duze
            {
                strbuff.setCharAt(i, (char)(strbuff.charAt(i)-32));
            }
        }
        return strbuff.toString();
    }
}
