package lab4;
import java.util.Scanner;

public class Zadanie1_a {
    
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj znak: ");
        char znak = scan.next().charAt(0);
        String zdanie = "AaAaAbBBb";
        System.out.println("Ilosc wystapien znaku \"" + znak + "\" w zdaniu \"" + zdanie + "\": " + countChar(zdanie, znak));  
    }
    public static int countChar(String str, char c)
    {
        int ilosc_wystapien = 0;
        
        for(int i = 0; i < str.length(); i++)
        {
            if(str.charAt(i) == c || str.charAt(i) == c+32 || str.charAt(i) == c-32)
            {
                ilosc_wystapien++;
            }
        }
        return ilosc_wystapien;
    }
}
