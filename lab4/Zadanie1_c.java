package lab4;
import java.util.Scanner;

public class Zadanie1_c {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj zdanie: ");
        String zdanie = scan.nextLine();
        System.out.println("Dla zdania: \"" + zdanie + "\" srodkowa czescia jest: \"" + middle(zdanie) + "\"");
    }
    
    public static String middle(String str)
    {
        int idx;
        if(str.length() % 2 == 0)
        {
            idx = (str.length() / 2) - 1;   
            int idx2 = idx + 1;  

            String lewy = String.valueOf(str.charAt(idx));   
            String prawy = String.valueOf(str.charAt(idx2));   
            return lewy + prawy;
        }
        else if(str.length() % 2 != 0)
        {
            idx = (int)Math.ceil(str.length()/2);
            return String.valueOf(str.charAt(idx));
        }
        return "";
    }
}
