package lab4;

public class Zadanie3 {
    public static void main(String[] args) {

        if(args.length < 2) {
            System.out.print("Przekazano za malo argumentow!");
            System.exit(-1);
        }

        int ilosc_wystapien = 0;

        if (args[0].contains(args[1])) {
            ilosc_wystapien = countSubStr(args[0], args[1]);
        }

        System.out.println("Ilosc wystapien \"" + args[1] + "\" w " + "\"" + args[0] + "\"" + " = " + ilosc_wystapien);
    }

    public static int countSubStr(String str, String subStr)
    {
        int ilosc_wystapien = 0;

        if(str == null || str.length() == 0 || subStr == null || subStr.length() == 0) {
            return ilosc_wystapien;
        }

        int index = 0;
        while (true) {
            index = str.indexOf(subStr, index);
            if (index != -1) { // zwraca -1 jezeli nie ma takiego subStringa w stringu
                ilosc_wystapien++;
                index += subStr.length();
            }
            else break;
        }
        return ilosc_wystapien;
    }
}
