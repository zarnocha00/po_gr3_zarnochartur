package lab4;

import java.math.BigInteger;

public class Zadanie4 {
    public static void main(String[] args) {

        if(args.length < 1) {
            System.out.print("Przekazano za malo argumentow!");
            System.exit(-1);
        }

        int n = Integer.parseInt(args[0]);
        if(n < 0) {
            System.out.print("Przekazano za maly argument!");
            System.exit(-1);
        }
        System.out.print("Liczba ziarenek na szachownicy " + n + " x " + n + " = " + szachownica(n));
    }

    static BigInteger szachownica(int n) {
        if(n <= 0) return BigInteger.valueOf(0);

        BigInteger bigInt = new BigInteger("1");
        for (int i = 1; i < (n*n); i++) {
            bigInt = bigInt.multiply(BigInteger.valueOf(2));

        }
        return bigInt;
    }
}
