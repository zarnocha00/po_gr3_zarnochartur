package pl.imiajd.zarnoch;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

public class Osoba_lab8 {
    private String nazwisko;
//  private int rokUrodzenia;
    String[] imiona;
    LocalDate dataUrodzenia;
    boolean plec;

    public Osoba_lab8() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj nazwisko: ");
        this.nazwisko = scanner.nextLine();

//      System.out.print("Podaj rok urodzenia: ");
//      this.rokUrodzenia = scanner.nextInt();

        System.out.println("Podaj imię/ imiona (jeżeli skończył*ś dodawanie imion - napisz '.')");
//      String imie_tmp = null;
        ArrayList<String> imiona_array = new ArrayList<>();

        while(true) {

            System.out.print("Podaj imie: ");
            String imie_tmp = scanner.next();
            if(!imie_tmp.equals(".")) imiona_array.add(imie_tmp);
            else break;

        }

        this.imiona = imiona_array.toArray(new String[0]);

        System.out.print("Podaj date urodzenia: ");
        this.dataUrodzenia = dodajDate(scanner);

        System.out.print("Czy jestes mezczyzna? [Odpowiedz: True/ False]: ");
        this.plec = scanner.nextBoolean();
    }

//z rokiemUrodzenia
//    public Osoba(String nazwisko, int rokUrodzenia, String[] imiona, LocalDate dataUrodzenia, boolean plec) {
//        this.nazwisko = nazwisko;
//        this.rokUrodzenia = rokUrodzenia;
//        this.imiona = imiona;
//        this.dataUrodzenia = dataUrodzenia;
//        this.plec = plec;
//    }

    public Osoba_lab8(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec) {

        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;

    }

    public String getNazwisko() {
        return nazwisko;
    }

//    public int getRokUrodzenia() {
//        return rokUrodzenia;
//    }

    public String[] getImiona() {
        return imiona;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public boolean getPlec() {
        return plec;
    }

//    public void setRokUrodzenia(int rokUrodzenia) {
//        this.rokUrodzenia = rokUrodzenia;
//    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setImiona(String[] imiona) {
        this.imiona = imiona;
    }

    public void setDataUrodzenia(LocalDate dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }

    public void setPlec(boolean plec) {
        this.plec = plec;
    }

    @Override
    public String toString() {

        String imiona_string = "";
        for (int i = 0; i < getImiona().length; i++) {
            if(i == (getImiona().length - 1)) {
                imiona_string += (getImiona()[i]);
            }
            else {
                imiona_string += (getImiona()[i] + ", ");
            }

        }

        String plec_osoby;
        if(getPlec()) plec_osoby = "mezczyzna";
        else plec_osoby = "kobieta";

//      return "\nImie/imiona: " + imiona_string + ", nazwisko: " + getNazwisko() + ", rok urodzenia: " + getRokUrodzenia() + ", data urodzenia: " + getDataUrodzenia() + ", plec: " + plec_osoby;
        return "\nImie/imiona: " + imiona_string + ", nazwisko: " + getNazwisko() + ", data urodzenia: " + getDataUrodzenia() + ", plec: " + plec_osoby;
    }

    public LocalDate dodajDate(Scanner scan) {

//      return LocalDate.parse(scan.next());
        System.out.print("Wprowadz date w formacie dzien.miesiac.rok: ");
        String data = scan.next();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        return LocalDate.parse(data, dtf);

    }
}
