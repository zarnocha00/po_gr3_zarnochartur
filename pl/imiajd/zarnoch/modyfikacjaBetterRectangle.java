package pl.imiajd.zarnoch;

public class modyfikacjaBetterRectangle extends java.awt.Rectangle {

    public modyfikacjaBetterRectangle(int A, int B) {
        super(A, B);
    }

    double getPerimeter() {
        return 2 * (getWidth() + getHeight());
    }

    double getArea() {
        return getWidth() * getHeight();
    }

}
