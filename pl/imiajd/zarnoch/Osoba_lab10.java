package pl.imiajd.zarnoch;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Osoba_lab10 implements Cloneable, Comparable {

    public Osoba_lab10() {
        System.out.print("Podaj nazwisko: ");
        Scanner scan = new Scanner(System.in);
        setNazwisko(scan.nextLine());

        System.out.print("Podaj date urodzenia w formacie yyyy-MM-dd: ");
        String data = scan.next();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        setDataUrodzenia(LocalDate.parse(data, dtf));
    }

    public Osoba_lab10(LocalDate dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;

        System.out.print("Podaj nazwisko:");
        Scanner scan = new Scanner(System.in);
        setNazwisko(scan.nextLine());
    }

    public Osoba_lab10(String nazwisko) {
        this.nazwisko = nazwisko;

        System.out.print("Podaj date urodzenia w formacie yyyy-MM-dd:");
        Scanner scan = new Scanner(System.in);
        String data = scan.next();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        setDataUrodzenia(LocalDate.parse(data, dtf));

    }

    public Osoba_lab10(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    private String nazwisko;
    private LocalDate dataUrodzenia;

    @Override
    public String toString() {

        if (this.getNazwisko() == null && this.getDataUrodzenia() == null) {
            return this.getClass().getSimpleName();
        }

        if (this.getNazwisko() != null && this.getDataUrodzenia() ==  null) {
            return (this.getClass().getSimpleName() + "[" + this.getNazwisko() + ", " + "---" + "]");
        }

        if (this.getNazwisko() == null && this.getDataUrodzenia() !=  null) {
            return (this.getClass().getSimpleName() + "[" + "---" + ", " + this.getDataUrodzenia().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))) + "]";
        }

        return this.getClass().getSimpleName() + "[" + this.getNazwisko() + ", " + this.getDataUrodzenia().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "]";

    }

    @Override
    public boolean equals(Object object) {
        if ((object == null) || (object.getClass() != this.getClass())) {
            return false;
        }

        if(object == this) {
            return true;
        }

        Osoba_lab10 osoba = (Osoba_lab10) object;

        if (this.getNazwisko() == null && osoba.getNazwisko() == null) {
            return this.getDataUrodzenia().equals(osoba.getDataUrodzenia());
        }

        else if (this.getNazwisko() == null || osoba.getNazwisko() == null) {
            return false;
        }

        if (this.getDataUrodzenia() == null && osoba.getDataUrodzenia() == null) {
            return this.getNazwisko().equalsIgnoreCase(osoba.getNazwisko());
        }

        else if (this.getDataUrodzenia() == null || osoba.getDataUrodzenia() == null) {
                return false;
        }



        if (this.getNazwisko().equalsIgnoreCase(osoba.getNazwisko())) {
            return this.getDataUrodzenia().equals(osoba.getDataUrodzenia());
        }
        return false;

//        return (osoba.getNazwisko().equalsIgnoreCase(this.getNazwisko()) && osoba.getDataUrodzenia() == this.getDataUrodzenia());
    }

    public String getNazwisko() {
        return this.nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return this.dataUrodzenia;
    }

    public void setDataUrodzenia(LocalDate dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }

    @Override
    public int compareTo(Object obj2) {

        if(this == obj2) {
            //System.out.println("Obiekty sa identyczne");
            return 0;
        }
//        Osoba_lab10 osoba1 = (Osoba) this;
        Osoba_lab10 osoba2 = (Osoba_lab10) obj2;

        if (this.getNazwisko() == null || this.dataUrodzenia == null || osoba2.getNazwisko() == null || osoba2.getDataUrodzenia() == null) {
            System.out.println("Nieporownywalne.");
            return 0;
        }

        if(this.getNazwisko().length() != osoba2.getNazwisko().length()) {
            //System.out.println("Roznica w nazwiskach");
            return (this.getNazwisko().length() - osoba2.getNazwisko().length());
        }
        else {
            if (this.getDataUrodzenia().equals(osoba2.getDataUrodzenia())) {
                return 0;
            }
            else {
                int roznica = this.getDataUrodzenia().getYear() - osoba2.getDataUrodzenia().getYear();

                if (roznica != 0) {
                    //System.out.println("Roznica w latach");
                }
                    if(roznica == 0) {
                        roznica = this.getDataUrodzenia().getMonthValue() - osoba2.getDataUrodzenia().getMonthValue();

                        if (roznica != 0) {
                            //System.out.println("Roznica w miesiacach");
                        }

                        if(roznica == 0) {
                            roznica = this.getDataUrodzenia().getDayOfMonth() - osoba2.getDataUrodzenia().getDayOfMonth();
                            if (roznica != 0) {
                                //System.out.println("Roznica w dniach");
                            }
                            return roznica;
                        }
                }
                return roznica;
            }
        }
    }

}
