package pl.imiajd.zarnoch;

import java.util.Scanner;

public class Student_lab7 extends Osoba_lab7 {

    private String kierunek;

    public Student_lab7() {
        super();
        System.out.print("Podaj kierunek: ");
        Scanner scanner = new Scanner(System.in);
        this.kierunek = scanner.nextLine();
    }

    public Student_lab7(String nazwisko, int rokUrodzenia, String kierunek) {
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }

    public void setKierunek(String kierunek) {
        this.kierunek = kierunek;
    }

    public String getKierunek() {
        return kierunek;
    }

    @Override
    public String toString() {
        return super.toString() + ", kierunek studiów: " + getKierunek();
    }
}
