package pl.imiajd.zarnoch;

import java.util.Scanner;

public class Nauczyciel_lab7 extends Osoba_lab7 {

    private float pensja;

    public Nauczyciel_lab7() {
        super();
        System.out.print("Podaj pensję: ");
        Scanner scanner = new Scanner(System.in);
        this.pensja = scanner.nextFloat();
    }

    public Nauczyciel_lab7(String nazwisko, int rokUrodzenia, float pensja) {

        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;

    }
    public float getPensja() {
        return pensja;
    }

    public void setPensja(float pensja) {
        this.pensja = pensja;
    }

    @Override
    public String toString() {
        return super.toString() + ", pensja: " + getPensja();
    }

}