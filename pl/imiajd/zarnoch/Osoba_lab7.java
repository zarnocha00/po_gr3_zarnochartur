package pl.imiajd.zarnoch;

import java.util.Scanner;

public class Osoba_lab7 {
    private String nazwisko;
    private int rokUrodzenia;

    public Osoba_lab7() {
        System.out.print("Podaj nazwisko: ");
        Scanner scanner = new Scanner(System.in);
        this.nazwisko = scanner.nextLine();

        System.out.print("Podaj rok urodzenia: ");
        this.rokUrodzenia = scanner.nextInt();
    }

    public Osoba_lab7(String nazwisko) {
        this.nazwisko = nazwisko;
        System.out.println("Podaj rok urodzenia: ");
        Scanner scanner = new Scanner(System.in);
        this.rokUrodzenia = scanner.nextInt();
    }

    public Osoba_lab7(int rokUrodzenia) {
        this.rokUrodzenia = rokUrodzenia;
        System.out.println("Podaj nazwisko: ");
        Scanner scanner = new Scanner(System.in);
        this.nazwisko = scanner.nextLine();
    }

    public Osoba_lab7(String nazwisko, int rokUrodzenia) {
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getRokUrodzenia() {
        return rokUrodzenia;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setRokUrodzenia(int rokUrodzenia) {
        this.rokUrodzenia = rokUrodzenia;
    }

    // do poprawy
    @Override
    public String toString() {
        return "Nazwisko: " + getNazwisko() + ", rok urodzenia: " + getRokUrodzenia();
    }
}
