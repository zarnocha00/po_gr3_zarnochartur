package pl.imiajd.zarnoch;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

// podmieniona klasa Nauczyciel
public class Pracownik_lab8 extends Osoba_lab8 {

    private float pensja;
    LocalDate dataZatrudnienia;

    public Pracownik_lab8() {

        super();

        System.out.print("Podaj pensję (z przecinkiem): ");
        Scanner scanner = new Scanner(System.in);
        this.pensja = scanner.nextFloat();

        System.out.print("Podaj date zatrudnienia: ");
        this.dataZatrudnienia = super.dodajDate(scanner);

    }

    public Pracownik_lab8(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, float pensja, LocalDate dataZatrudnienia) {

        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pensja = pensja;
        this.dataZatrudnienia = dataZatrudnienia;

    }
    public float getPensja() {
        return pensja;
    }

    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    public void setPensja(float pensja) {
        this.pensja = pensja;
    }

    public void setDataZatrudnienia(LocalDate dataZatrudnienia) {
        this.dataZatrudnienia = dataZatrudnienia;
    }

    @Override
    public String toString() {
        return super.toString() + ", pensja: " + getPensja() + ", data zatrudnienia: " + getDataZatrudnienia();
    }

}