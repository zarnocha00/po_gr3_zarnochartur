package pl.imiajd.zarnoch;

public interface Comparable<T> {
    int compareTo(T obj2);
}
