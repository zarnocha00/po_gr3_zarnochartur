package pl.imiajd.zarnoch;

import java.time.LocalDate;
import java.util.Scanner;

public class Student_lab10 extends Osoba_lab10 implements Comparable, Cloneable {

    private double sredniaOcen;

    public Student_lab10(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    public Student_lab10(LocalDate dataUrodzenia, double sredniaOcen) {
        super(dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    public Student_lab10(String nazwisko, double sredniaOcen) {
        super(nazwisko);
        this.sredniaOcen = sredniaOcen;
    }

    public Student_lab10(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    public Student_lab10() {
        super();
        System.out.print("Podaj srednia ocen (po przecinku): ");
        Scanner scan = new Scanner(System.in);
        setSredniaOcen(scan.nextDouble());
    }

    public Student_lab10(LocalDate dataUrodzenia) {
        super(dataUrodzenia);
        System.out.print("Podaj srednia ocen (po przecinku): ");
        Scanner scan = new Scanner(System.in);
        setSredniaOcen(scan.nextDouble());
    }

    public Student_lab10(String nazwisko) {
        super(nazwisko);
        System.out.print("Podaj srednia ocen (wartosci po przecinku): ");
        Scanner scan = new Scanner(System.in);
        setSredniaOcen(scan.nextDouble());
    }

    public Student_lab10(String nazwisko, LocalDate dataUrodzenia) {
        super(nazwisko, dataUrodzenia);
        System.out.print("Podaj srednia ocen (po przecinku): ");
        Scanner scan = new Scanner(System.in);
        setSredniaOcen(scan.nextDouble());
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public String toString() {
        return super.toString() + " [srednia: " + getSredniaOcen() + "]";
    }

    @Override
    public int compareTo(Object obj2) {

        if(this == obj2) {
            //System.out.println("Obiekty sa identyczne");
            return 0;
        }
//        Osoba osoba1 = (Osoba) this;
        Student_lab10 s = (Student_lab10) obj2;

        if (this.getNazwisko() == null || this.getDataUrodzenia() == null || ((Student_lab10) obj2).getNazwisko() == null || ((Student_lab10) obj2).getDataUrodzenia() == null) {
            System.out.println("Nieporownywalne.");
            return 0;
        }

        if(this.getNazwisko().length() != s.getNazwisko().length()) {
            //System.out.println("Roznica w nazwiskach");
            return (this.getNazwisko().length() - s.getNazwisko().length());
        }
        else {
            if (this.getDataUrodzenia().equals(s.getDataUrodzenia())) {
                return 0;
            }
            else {
                int roznica = this.getDataUrodzenia().getYear() - s.getDataUrodzenia().getYear();

                if(roznica == 0) {
                    roznica = this.getDataUrodzenia().getMonthValue() - s.getDataUrodzenia().getMonthValue();

                    if(roznica == 0) {
                        roznica = this.getDataUrodzenia().getDayOfMonth() - s.getDataUrodzenia().getDayOfMonth();
                        if (roznica == 0) {
                            return (int)(this.getSredniaOcen() - s.getSredniaOcen());
                        }
                        return roznica;
                    }
                }
                return roznica;
            }
        }
    }

}
