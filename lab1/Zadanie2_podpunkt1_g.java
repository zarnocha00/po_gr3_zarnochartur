/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1;

import java.util.Scanner;

/**
 *
 * @author Artur
 */
public class Zadanie2_podpunkt1_g {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Podaj n: ");
        
        int n = scan.nextInt();
        int ilosc = 0;
        
        int[] tab = new int[n];
        int wprowadzona; 
        
        // wypelnianie tablicy:
        for(int i = 0; i < n; i++)
        {
            System.out.print("Podaj " + (i+1) + ". liczbe: ");  
            wprowadzona = scan.nextInt();
            tab[i] = wprowadzona;            
        }
        
        for(int i = 0; i < n; i++)
        {
            if(tab[i] % 2 != 0 && 0 <= tab[i])
            {
                ilosc++;
            }
        }
        System.out.println("\nliczb bedacych kwadratami liczby parzystej jest: " + ilosc);
    }
}
