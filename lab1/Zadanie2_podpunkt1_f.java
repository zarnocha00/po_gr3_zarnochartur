/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1;

import java.util.Scanner;

/**
 *
 * @author Artur
 */
public class Zadanie2_podpunkt1_f {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Podaj n: ");
        
        int n = scan.nextInt();
        int ilosc = 0;
                
        int[] tab = new int[n];
        int wprowadzona; 
        
        // wypelnianie tablicy:
        for(int i = 0; i < n; i++)
        {
            System.out.print("Podaj " + (i+1) + ". liczbe: ");  
            wprowadzona = scan.nextInt();
            tab[i] = wprowadzona;            
        }
        
        for(int i = 0; i < n; i++)
        {
            if(tab[i] % 2 == 0 && (i+1) % 2 != 0)
            {
                ilosc++;
            }
        }
        System.out.println("\nliczb o nieparzystym indeksie bedacymi liczbami parzystymi jest: " + ilosc);
    }
}
