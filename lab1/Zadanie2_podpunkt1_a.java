/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1;

import java.util.Scanner;

/**
 *
 * @author local
 */
public class Zadanie2_podpunkt1_a {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Podaj n: ");
        
        int n = scan.nextInt();
        int ilosc_nieparzystych = 0;
        
        int[] tab = new int[n];
        int wprowadzona; 
        
        for(int i = 1; i <= n; i++)
        {
            System.out.print("Podaj " + i + ". liczbe: ");  
            wprowadzona = scan.nextInt();
            tab[(i-1)] = wprowadzona;
            //System.out.print("Kolejne liczby naturalne ktore sa nieparzyste: ")
            if(wprowadzona % 2 == 1)
            {
                ilosc_nieparzystych++;
            }
        }
        System.out.println("\nliczb nieparzystych jest: " + ilosc_nieparzystych + "\n");
    }
}
