/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1;

import java.util.Scanner;

/**
 *
 * @author local
 */
public class Zadanie1_b {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Podaj n: ");
        
        int n = scan.nextInt();
        float iloczyn = 1;
        
        
        for(int i = 0; i < n; i++)
        {
            System.out.print("Podaj " + (i+1) + " liczbe: ");
            float wczytywana = scan.nextFloat();
            iloczyn *= wczytywana;
        }
        System.out.println("Iloczyn wynosi: " + iloczyn);
    }
}
