/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1;

import java.util.Scanner;

/**
 *
 * @author local
 */
public class Zadanie2_podpunkt1_b {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Podaj n: ");
        
        int n = scan.nextInt();
        int ilosc = 0;
        
        int[] tab = new int[n];
        int wprowadzona; 
        
        for(int i = 1; i <= n; i++)
        {
            System.out.print("Podaj " + i + ". liczbe: ");  
            wprowadzona = scan.nextInt();
            tab[(i-1)] = wprowadzona;
            //System.out.print("Kolejne liczby naturalne ktore sa nieparzyste: ")
            if(wprowadzona % 3 == 0 && wprowadzona % 5 != 0)
            {
                ilosc++;
            }
        }
        System.out.println("\nliczb podzielnych przez 3 i jednoczesnie niepodzielnych przez 5 jest: " + ilosc);
    }
}
