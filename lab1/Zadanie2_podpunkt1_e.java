/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1;

import java.util.Scanner;

/**
 *
 * @author Artur
 */
public class Zadanie2_podpunkt1_e {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Podaj n: ");
        
        int n = scan.nextInt();
        int ilosc = 0;
        

        int[] tab = new int[n];
        int wprowadzona; 
        
        // wypelnianie tablicy:
        for(int i = 0; i < n; i++)
        {
            System.out.print("Podaj " + (i+1) + ". liczbe: ");  
            wprowadzona = scan.nextInt();
            tab[i] = wprowadzona;            
        }
        
        //System.out.println("\n");
        // tutaj warunek z zadania:
        for(int k = 1; k <= n; k++)
        {
            /*System.out.println("k: " + k);
            System.out.println("Potega: " + Math.pow(2, k));
            System.out.println("Silnia: " + silnia(k) + "\n");*/
            if(Math.pow(2, k) < tab[k-1] && tab[k-1] < silnia(k)) //dla k = 1/ 2/ 3 ten warunek nie bedzie spelniony dla jakiegokolwiek tab[k-1]
            {
                ilosc++;
            }
        }
        System.out.println("\nliczb spelniajacych ten warunek jest: " + ilosc);
    }
    
    
    public static int silnia (int n) {
        int iloczyn = 1;
        
        for (int i = 1; i <= n; i++) {
            iloczyn *= i;
        }
        
        return iloczyn;
    }
}
