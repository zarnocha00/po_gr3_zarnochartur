/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1;

import java.util.Scanner;

/**
 *
 * @author local
 */
public class Zadanie1_h {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Podaj n: ");
        
        int n = scan.nextInt();
        double suma = 0;
        
        
        for(int i = 0; i < n; i++)
        {
            System.out.print("Podaj " + (i+1) + " liczbe: ");
            double wczytywana = scan.nextDouble();
            
            wczytywana *= Math.pow(-1, i);
            suma += wczytywana;
        }
        System.out.println("Obliczenie wynosi: " + suma);
    }
}
