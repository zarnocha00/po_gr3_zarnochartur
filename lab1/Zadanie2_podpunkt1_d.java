/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1;

import java.util.Scanner;

/**
 *
 * @author Artur
 */
public class Zadanie2_podpunkt1_d {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Podaj n: ");
        
        int n = scan.nextInt();
        int ilosc = 0;
        
        int[] tab = new int[n];
        int wprowadzona; 
        
        // przepraszam za zarzut, że zadanie nie ma sensu - miałem widoczny problem ze zrozumieniem koncepcji polecenia :-)
        
        System.out.print("\n");
        
        // wypelnianie tablicy:
        for(int i = 0; i < n; i++)
        {
            System.out.print("Podaj " + (i+1) + ". liczbe: ");  
            wprowadzona = scan.nextInt();
            tab[i] = wprowadzona;            
        }
        
        // warunek z zadania:
        for(int i = 0; i < tab.length; i++)
        {
            if(i > 0 && i < (tab.length - 1) )
            {
                /*System.out.println("\ni: " + i);
                System.out.println("tab.length - 1: " + (tab.length - 1));
                System.out.println("tab[i]: " + tab[i]);
                System.out.println("tab[i-1]: " + tab[i-1]);
                System.out.println("tab[i+1]: " + tab[i+1]);*/
                    
                if (tab[i] < ((tab[i-1] + tab[i+1]) / 2)) 
                {
                    //System.out.println("Dodano element");
                    ilosc++;
                }
            }
        }
        
        System.out.println("\nliczb spelniajacych ten warunek jest: " + ilosc);
    }
}
