package lab1;

import java.util.Scanner;

/**
 *
 * @author Artur
 */


public class Zadanie3 {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Podaj n: ");
        
        int n = scan.nextInt();
        
        double[] tab = new double[n];
        double wprowadzona; 
        
        // wypelnianie tablicy:
        for(int i = 0; i < n; i++)
        {
            System.out.print("Podaj " + (i+1) + ". liczbe: ");  
            wprowadzona = scan.nextDouble();
            tab[i] = wprowadzona;            
        }
        
        // warunek z zadania:
        int dodatnie = 0;
        int zera = 0;
        int ujemne = 0;
        
        for(int j = 0; j < n; j++)
        {
            if(tab[j] > 0)
            {
                dodatnie ++;
            }
            else if(tab[j] == 0)
            {
                zera++;
            }
            else ujemne++;
        }
        
        System.out.println("\nDodatnich liczb jest: " + dodatnie);
        System.out.println("Zer jest: " + zera);
        System.out.println("Ujemnych liczb jest: " + ujemne);
        
    }
}
