package lab1;

import java.util.Scanner;

/**
    @author Artur
 */

public class Zadanie5 {
   public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Podaj n: ");
        
        int n = scan.nextInt();
        
        double[] tab = new double[n];
        double wprowadzona; 
        
        // wypelnianie tablicy:
        for(int i = 0; i < n; i++)
        {
            System.out.print("Podaj " + (i+1) + ". liczbe: ");  
            wprowadzona = scan.nextDouble();
            tab[i] = wprowadzona;            
        }
        
        // warunek z zadania:
        
        int dodatnie_pary = 0;
        System.out.print("\n");
        for(int i = 0, j = 1; i < tab.length && j < tab.length; i++, j++)
        {
            if(tab[i] > 0 && tab[j] > 0)
            {
                System.out.println("(" + tab[i] + ", " + tab[j] + ")");
                dodatnie_pary++;
            }
        }
        System.out.println("\nDodatnich par jest: " + dodatnie_pary);   
    } 
}
