/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1;

import java.util.Scanner;

/**
 *
 * @author local
 */
public class Zadanie1_2 {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Podaj n: ");
        
        int n = scan.nextInt();
        float tablica[] = new float[n];
        
        for(int i = 0; i < n; i++)
        {
            System.out.print("Podaj " + (i+1) + " liczbe: ");
            float wczytywana = scan.nextFloat();
            
            tablica[i] = wczytywana;
        }
         
        //przesuniecie tablicy:
        float[] przesunieta_tablica = new float[tablica.length];
        
        for(int i=1; i<(tablica.length); i++)
        {
            przesunieta_tablica[(i-1)] = tablica[i];
        }
        //dodaie pierwszego elementu na koniec przesunietej tablicy:
        przesunieta_tablica[(przesunieta_tablica.length - 1)] = tablica[0];
        
        //wypisanie tablic:
        wypisz(tablica);
        wypisz(przesunieta_tablica);
    }
    
    public static void wypisz(float[] tab){
        System.out.print("\n");
        for(int i=0; i<tab.length; i++)
        {
            System.out.print(tab[i] + " ");
        }
        System.out.print("\n");
    }
    
}
