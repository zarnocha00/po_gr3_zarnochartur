package lab1;

import java.util.Scanner;

/**
    @author Artur
 */

public class Zadanie2 {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Podaj n: ");
        
        int n = scan.nextInt();
        
        double[] tab = new double[n];
        double wprowadzona; 
        
        // wypelnianie tablicy:
        for(int i = 0; i < n; i++)
        {
            System.out.print("Podaj " + (i+1) + ". liczbe: ");  
            wprowadzona = scan.nextDouble();
            tab[i] = wprowadzona;            
        }
        
        // warunek z zadania:
        int suma = 0;
        
        for(int j = 0; j < n; j++)
        {
            if(tab[j] > 0)
            {
                suma += tab[j];
            }
        }
        
        suma *= 2;
        System.out.println("\nPodwojona suma dodatnich liczb: " + suma);   
    }
}
