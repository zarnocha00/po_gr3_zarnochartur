package lab1;

import java.util.Scanner;

/**
 *
 * @author Artur
 */
public class Zadanie4 {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Podaj n: ");
        
        int n = scan.nextInt();
        
        double[] tab = new double[n];
        double wprowadzona; 
        
        // wypelnianie tablicy:
        for(int i = 0; i < n; i++)
        {
            System.out.print("Podaj " + (i+1) + ". liczbe: ");  
            wprowadzona = scan.nextDouble();
            tab[i] = wprowadzona;            
        }
        
        // warunek z zadania:

        double najmniejsza = tab[0];
        double najwieksza = tab[0];
        
        for(int j = 0; j < n; j++)
        {
            if(tab[j] < najmniejsza)
            {
                najmniejsza = tab[j];
            }
            else if(tab[j] > najwieksza)
            {
                najwieksza = tab[j];
            }
        }
        
        System.out.println("\nNajmniejsza wprowadzona liczba: " + najmniejsza);
        System.out.println("Najwieksza wprowadzona liczba: " + najwieksza);
    }
}
