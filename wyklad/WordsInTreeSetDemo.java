// Na podstawie książki Cay Horstmann, Gary Cornell.
// "Java (TM). Podstawy." Wydanie VIII

// Program wykorzystujący zbiór typu TreeSet
// w celu pokazania wszystkich słów odczytanych z System.in

package wyklad;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class WordsInTreeSetDemo
{
    public static void main(String[] args) throws FileNotFoundException {
        // Klasa TreeSet implementuje interfejs Set
        Set<String> words = new TreeSet<>();
        long totalTime = 0;
        System.out.print("Podaj lokalizacje pliku: ");
        String lokalizacja = new Scanner(System.in).nextLine();
        Scanner in = new Scanner(new File(lokalizacja));
//        Scanner in = new Scanner(new File("wyklad/alice30.txt"));

        while (in.hasNext()) {
            String word = in.next();
            long callTime = System.currentTimeMillis();
            words.add(word);
            callTime = System.currentTimeMillis() - callTime;
            totalTime += callTime;
        }

        Iterator<String> iter = words.iterator();
        for (int j = 1; j <= 20; ++j) {
            System.out.println(iter.next());
        }

        System.out.println("TreeSet:");
        System.out.println(words.size() + " unikalnych słów. " + totalTime + " milisekund.");
    }
}

