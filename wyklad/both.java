package wyklad;

import java.io.FileNotFoundException;

public class both {

    public static void main(String[] args) throws FileNotFoundException {
        WordsInHashSetDemo.main(new String[]{});
        System.out.println("");
        WordsInTreeSetDemo.main(new String[]{});

        /*Dla pliku 'lorem_ipsum6000' zawierajacego 6 000 slow (458 unikalne), czas pracy:
        // lokalizacja: wyklad\lorem_ipsum6000.txt

                > HashSet: 8 milisekund;
                > TreeSet: 1 milisekund.

                > HashSet: 15 milisekund;
                > TreeSet: 17 milisekund.

                > HashSet: 1 milisekund;
                > TreeSet: 3 milisekund.

        Dla pliku 'lorem_ipsum6000' zawierajacego 6 000 slow (458 unikalne), czas pracy:
        // lokalizacja: wyklad\lorem_ipsum60000.txt

                > HashSet: 9 milisekund;
                > TreeSet: 11 milisekund.

                > HashSet: 26 milisekund;
                > TreeSet: 11 milisekund.

                > HashSet: 25 milisekund;
                > TreeSet: 38 milisekund.

        Dla pliku 'polskie_lorem_ipsum' zawierajacego 13 204 slow (458 unikalne), czas pracy:
        // lokalizacja: wyklad\polskie_lorem_ipsum.txt

                > HashSet: 5 milisekund;
                > TreeSet: 16 milisekund.

                > HashSet: 8 milisekund;
                > TreeSet: 8 milisekund.

                > HashSet: 0 milisekund;
                > TreeSet: 15 milisekund.

          Dla pliku 'alice_30' zawierajacego 56 274 slowa (5 908 unikalne), czas pracy:
          // lokalizacja: wyklad\alice30.txt

                > HashSet: 0 milisekund;
                > TreeSet: 31 milisekund.

                > HashSet: 16 milisekund;
                > TreeSet: 31 milisekund.

                > HashSet: 40 milisekund;
                > TreeSet: 35 milisekund.

            Wniosek: moim zdaniem przeważa szybkość HashSetu, 8 razy był lepszy od TreeSetu, który zwyciężył jedynie 3 razy. Raz był remis.
        */
    }

}
