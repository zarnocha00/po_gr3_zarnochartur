package lab7;

import java.util.Scanner;

public class Zadanie6 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj boki prostokata: ");
        BetterRectangle prostokat = new BetterRectangle(scanner.nextInt(), scanner.nextInt());
        System.out.println("Obwod: " + prostokat.getPerimeter());
        System.out.println("Pole: " + prostokat.getArea());

        /*Zadanie 7*/
        System.out.print("\nPodaj boki prostokata: ");
        modyfikacjaBetterRectangle modyfikacja_prostokat = new modyfikacjaBetterRectangle(scanner.nextInt(), scanner.nextInt());
        System.out.println("Obwod: " + modyfikacja_prostokat.getPerimeter());
        System.out.println("Pole: " + modyfikacja_prostokat.getArea());

    }
}