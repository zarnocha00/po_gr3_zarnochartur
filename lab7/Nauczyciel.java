package lab7;

import java.util.Scanner;

public class Nauczyciel extends Osoba {

    private float pensja;

    public Nauczyciel() {
        super();
        System.out.print("Podaj pensję: ");
        Scanner scanner = new Scanner(System.in);
        this.pensja = scanner.nextFloat();
    }

    public Nauczyciel(String nazwisko, int rokUrodzenia, float pensja) {

        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;

    }
    public float getPensja() {
        return pensja;
    }

    public void setPensja(float pensja) {
        this.pensja = pensja;
    }

    @Override
    public String toString() {
        return super.toString() + ", pensja: " + getPensja();
    }

}