package lab7;

import java.time.LocalDate;
import java.util.Scanner;

public class Osoba {
    private String nazwisko;
    private int rokUrodzenia;

    public Osoba() {
        System.out.print("Podaj nazwisko: ");
        Scanner scanner = new Scanner(System.in);
        this.nazwisko = scanner.nextLine();

        System.out.print("Podaj rok urodzenia: ");
        this.rokUrodzenia = scanner.nextInt();
    }

    public Osoba(String nazwisko) {
        this.nazwisko = nazwisko;
        System.out.println("Podaj rok urodzenia: ");
        Scanner scanner = new Scanner(System.in);
        this.rokUrodzenia = scanner.nextInt();
    }

    public Osoba(int rokUrodzenia) {
        this.rokUrodzenia = rokUrodzenia;
        System.out.println("Podaj nazwisko: ");
        Scanner scanner = new Scanner(System.in);
        this.nazwisko = scanner.nextLine();
    }

    public Osoba(String nazwisko, int rokUrodzenia) {
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }

    public Osoba(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec) {
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getRokUrodzenia() {
        return rokUrodzenia;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setRokUrodzenia(int rokUrodzenia) {
        this.rokUrodzenia = rokUrodzenia;
    }

    // do poprawy
    @Override
    public String toString() {
        return "Nazwisko: " + getNazwisko() + ", rok urodzenia: " + getRokUrodzenia();
    }
}
