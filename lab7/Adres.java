package lab7;

public class Adres {
    String ulica;
    int numer_domu;
    int numer_mieszkania;
    String miasto;
    String kod_pocztowy;

    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = -1;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public void pokaz() {
        System.out.println(this.kod_pocztowy + ", " + this.miasto);

        if(this.numer_mieszkania == -1) {
            System.out.println(this.ulica + ", " + this.numer_domu);
        }
        else {
            System.out.println(this.ulica + ", " + this.numer_domu + ", " + this.numer_mieszkania);
        }
    }

    public boolean przed(Adres arg_adres) {
        int this_kp = Integer.parseInt(this.kod_pocztowy.replace("-","" ));
        int arg_adres_kp = Integer.parseInt(arg_adres.kod_pocztowy.replace("-","" ));

        return this_kp < arg_adres_kp;
    }
}
