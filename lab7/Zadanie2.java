package lab7;

public class Zadanie2 {
    public static void main(String[] args) {
        Adres adr1 = new Adres("Szkolna", 1, "Białystok", "12-345");
        adr1.pokaz();

        Adres adr2 = new Adres("Słoneczna", 3, 8,"Olsztyn", "67-890");

        Adres adr3 = new Adres("Słoneczna", 3, 8,"Olsztyn", "67-890");

        System.out.println();
        adr2.pokaz();

        System.out.println("\n" + adr1.przed(adr2));
        System.out.println(adr3.przed(adr2));
        System.out.println(adr3.przed(adr1));
    }
}
