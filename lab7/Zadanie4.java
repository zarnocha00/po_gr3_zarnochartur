package lab7;

public class Zadanie4 {
    public static void main(String[] args) {

        Osoba nowa_osoba = new Osoba("Wyimaginowany", 1975);
        Student nowy_student = new Student("Studencki", 1999, "Prawo");
        Nauczyciel nowy_nauczyciel = new Nauczyciel("Teacherowski", 1977, 3333.33f);

        nowa_osoba.setNazwisko("Osobliwy"); nowa_osoba.setRokUrodzenia(1999);
        nowy_student.setNazwisko("Student"); nowy_student.setRokUrodzenia(2000); nowy_student.setKierunek("Informatyka");
        nowy_nauczyciel.setNazwisko("NauczyCie"); nowy_nauczyciel.setRokUrodzenia(1975); nowy_nauczyciel.setPensja(3900.99f);

        System.out.println("nowa_osoba: " + nowa_osoba);
        System.out.println("nowy_student: " + nowy_student);
        System.out.println("nowy_nauczyciel: " + nowy_nauczyciel);

        System.out.println("\nnowsza_osoba init: ");
        Osoba nowsza_osoba = new Osoba();

        System.out.println("\nnowszy_student init: ");
        Student nowszy_student = new Student();

        System.out.println("\nnowszy_nauczyciel init: ");
        Nauczyciel nowszy_nauczyciel = new Nauczyciel();

        System.out.println("\nnowsza_osoba: " + nowsza_osoba);
        System.out.println("nowszy_student: " + nowszy_student);
        System.out.println("nowszy_nauczyciel: " + nowszy_nauczyciel);

    }
}
