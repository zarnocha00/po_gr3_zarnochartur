package lab7;

import java.util.Scanner;

public class Student extends Osoba {

    private String kierunek;

    public Student() {
        super();
        System.out.print("Podaj kierunek: ");
        Scanner scanner = new Scanner(System.in);
        this.kierunek = scanner.nextLine();
    }

    public Student(String nazwisko, int rokUrodzenia, String kierunek) {
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }

    public void setKierunek(String kierunek) {
        this.kierunek = kierunek;
    }

    public String getKierunek() {
        return kierunek;
    }

    @Override
    public String toString() {
        return super.toString() + ", kierunek studiów: " + getKierunek();
    }
}
