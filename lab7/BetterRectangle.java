package lab7;

public class BetterRectangle extends java.awt.Rectangle {

    public BetterRectangle(int A, int B) {

        this.setLocation(A, B);
        this.setSize(A, B);

    }

    double getPerimeter() {
       return 2 * (getWidth() + getHeight());
    }

    double getArea() {
        return getWidth() * getHeight();
    }

}