package lab10;

import java.time.LocalDate;

public class test {
    public static void main(String[] args) {

        Osoba osoba = new Osoba("Burak", LocalDate.parse("2001-01-01"));
        System.out.println(osoba);

        Osoba osoba1 = new Osoba("Cukinia", null);
        System.out.println(osoba1);

        Osoba osoba2 = new Osoba(null, LocalDate.parse("2010-10-10"));
        System.out.println(osoba2);

        Osoba osoba3 = new Osoba("Bombel", LocalDate.parse("2010-10-12"));

        Osoba osoba4 = new Osoba("Bombel", LocalDate.parse("2010-10-10"));

        boolean ocena = osoba3.equals(osoba4);
        System.out.println("\n" + ocena + "\n");

        int dlugosc = osoba1.compareTo(osoba2);
        System.out.println(dlugosc);
    }
}
