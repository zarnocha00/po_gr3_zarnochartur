package lab10;

public interface Comparable<T> {
    int compareTo(T obj2);
}
