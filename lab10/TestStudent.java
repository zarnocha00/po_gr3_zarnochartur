package lab10;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestStudent {
    public static void main(String[] args) {

        ArrayList<Student> grupa = new ArrayList<>();

        Student s1 = new Student("Żarnoch", LocalDate.parse("2000-07-04"), 3.33);
        Student s2 = new Student("Bliźniak", LocalDate.parse("2000-12-05"), 5.29);
        Student s3 = new Student("Bliźniak", LocalDate.parse("2000-12-05"), 3.15);
        Student s4 = new Student("BT", LocalDate.parse("1990-04-07"), 3.15);
        Student s5 = new Student("UB", LocalDate.parse("1990-04-07"), 3.15);
        grupa.add(s1); grupa.add(s2); grupa.add(s3); grupa.add(s4); grupa.add(s5);

        System.out.println("B4 sort:");
        for (Student s : grupa) {
            System.out.println(s);
        }

        System.out.println("\nAfter sort:");
        grupa.sort(Student::compareTo);
        for (Student s : grupa) {
            System.out.println(s);
        }

    }
}
