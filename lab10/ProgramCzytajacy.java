package lab10;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class ProgramCzytajacy {
    public static void main(String[] args) throws FileNotFoundException {

        // $FileDir$\LoremIpsum.txt

        if (args.length == 0) {
            System.out.println("Brak argumentów wywołania programu");
            System.exit(0);
        }

        File plik = new File(args[0]);

        Scanner scan = new Scanner(plik);

        ArrayList<String> array = new ArrayList<>();

        while(scan.hasNext()) {
            String linia = scan.next();
            array.add(linia);
        }

        array.sort(Comparator.naturalOrder());
        System.out.println("Posortowany array:\n" + array);
        System.out.println("Array size: " + array.size());
    }
}
