package lab10;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestOsoba {
    public static void main(String[] args) {

        ArrayList<Osoba> grupa = new ArrayList<>();

        Osoba o1 = new Osoba("Zarnoch", LocalDate.parse("2000-07-04"));
        Osoba o2 = new Osoba("Bliźniak", LocalDate.parse("2000-12-10"));
        Osoba o3 = new Osoba("Bliźniak", LocalDate.parse("2000-12-05"));
        Osoba o4 = new Osoba("BT", LocalDate.parse("1990-04-07"));
        Osoba o5 = new Osoba("UB", LocalDate.parse("1990-04-07"));
        grupa.add(o1); grupa.add(o2); grupa.add(o3); grupa.add(o4); grupa.add(o5);

        System.out.println("B4 sort:");
        for (Osoba osoba : grupa) {
            System.out.println(osoba);
        }

        System.out.println("\nAfter sort:");
        grupa.sort(Osoba::compareTo);
        for (Osoba osoba : grupa) {
            System.out.println(osoba);
        }

    }
}
