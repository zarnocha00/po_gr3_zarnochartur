package lab10;

import java.time.LocalDate;
import java.util.Scanner;

public class Student extends Osoba implements Comparable, Cloneable {

    private double sredniaOcen;

    public Student(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    public Student(LocalDate dataUrodzenia, double sredniaOcen) {
        super(dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    public Student(String nazwisko, double sredniaOcen) {
        super(nazwisko);
        this.sredniaOcen = sredniaOcen;
    }

    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    public Student() {
        super();
        System.out.print("Podaj srednia ocen (po przecinku): ");
        Scanner scan = new Scanner(System.in);
        setSredniaOcen(scan.nextDouble());
    }

    public Student(LocalDate dataUrodzenia) {
        super(dataUrodzenia);
        System.out.print("Podaj srednia ocen (po przecinku): ");
        Scanner scan = new Scanner(System.in);
        setSredniaOcen(scan.nextDouble());
    }

    public Student(String nazwisko) {
        super(nazwisko);
        System.out.print("Podaj srednia ocen (wartosci po przecinku): ");
        Scanner scan = new Scanner(System.in);
        setSredniaOcen(scan.nextDouble());
    }

    public Student(String nazwisko, LocalDate dataUrodzenia) {
        super(nazwisko, dataUrodzenia);
        System.out.print("Podaj srednia ocen (po przecinku): ");
        Scanner scan = new Scanner(System.in);
        setSredniaOcen(scan.nextDouble());
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public String toString() {
        return super.toString() + " [srednia: " + getSredniaOcen() + "]";
    }

    @Override
    public int compareTo(Object obj2) {

        if(this == obj2) {
            //System.out.println("Obiekty sa identyczne");
            return 0;
        }
//        Osoba osoba1 = (Osoba) this;
        Student s = (Student) obj2;

        if (this.getNazwisko() == null || this.getDataUrodzenia() == null || s.getNazwisko() == null || s.getDataUrodzenia() == null) {
            System.out.println("Nieporownywalne.");
            return 0;
        }

        if(this.getNazwisko().length() != s.getNazwisko().length()) {
            //System.out.println("Roznica w nazwiskach");
            return (this.getNazwisko().length() - s.getNazwisko().length());
        }
        else {
            if (this.getDataUrodzenia().equals(s.getDataUrodzenia())) {
                return 0;
            }
            else {
                int roznica = this.getDataUrodzenia().getYear() - s.getDataUrodzenia().getYear();

                if(roznica == 0) {
                    roznica = this.getDataUrodzenia().getMonthValue() - s.getDataUrodzenia().getMonthValue();

                    if(roznica == 0) {
                        roznica = this.getDataUrodzenia().getDayOfMonth() - s.getDataUrodzenia().getDayOfMonth();
                        if (roznica == 0) {
                            return (int)(this.getSredniaOcen() - s.getSredniaOcen());
                        }
                        return roznica;
                    }
                }
                return roznica;
            }
        }
    }

}
