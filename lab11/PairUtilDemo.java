package lab11;

public class PairUtilDemo {
    public static void main(String[] args) {

        Pair p1 = new Pair();
        p1.setFirst("A");
        p1.setSecond(2);

        System.out.println("p1: " + p1.getFirst() + ", " + p1.getSecond());

        Pair p2;
        p2 = PairUtil.swap(p1);

        p1.swap();

        System.out.println("\nPo swap:");
        System.out.println("p1: " + p1.getFirst() + ", " + p1.getSecond());
        System.out.println("p2: " + p2.getFirst() + ", " + p2.getSecond());
    }

}
