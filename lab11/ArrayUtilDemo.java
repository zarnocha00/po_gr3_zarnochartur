package lab11;

import java.time.LocalDate;
import java.util.ArrayList;

public class ArrayUtilDemo {
    public static void main(String[] args) {

        ArrayList<Integer> intlist1 = new ArrayList<>();
        intlist1.add(5);
        intlist1.add(7);
        intlist1.add(1);
        intlist1.add(2);
        intlist1.add(1);

        System.out.println("ArrayUtil.isSorted(intlist1): " + ArrayUtil.isSorted(intlist1) + "\n");

        ArrayList<Integer> intlist2 = new ArrayList<>();
        intlist2.add(1);
        intlist2.add(2);
        intlist2.add(10);
        intlist2.add(11);

        System.out.println("ArrayUtil.isSorted(intlist2): " + ArrayUtil.isSorted(intlist2) + "\n");

        ArrayList<LocalDate> dates1 = new ArrayList<>();
        dates1.add(LocalDate.parse("2000-07-04"));
        dates1.add(LocalDate.parse("0966-12-24"));
        dates1.add(LocalDate.parse("2002-07-04"));
        dates1.add(LocalDate.parse("1990-03-12"));

        System.out.println("ArrayUtil.isSorted(dates1): " + ArrayUtil.isSorted(dates1) + "\n");

        ArrayList<LocalDate> dates2 = new ArrayList<>();
        dates2.add(LocalDate.parse("1919-10-06"));
        dates2.add(LocalDate.parse("1950-05-03"));
        dates2.add(LocalDate.parse("1996-06-06"));
        dates2.add(LocalDate.parse("2000-07-04"));
        System.out.println("ArrayUtil.isSorted(dates2): " + ArrayUtil.isSorted(dates2));
        ArrayList<Integer> bs_int = new ArrayList<>();
        bs_int.add(1);
        bs_int.add(3);
        bs_int.add(5);
        bs_int.add(9);
        bs_int.add(12);
        bs_int.add(15);
        bs_int.add(20);

        System.out.println("\nArrayUtil.binSearch(bs_int, 20): " + ArrayUtil.binSearch(bs_int, 20));

        ArrayList<LocalDate> bs_date = new ArrayList<>();
        bs_date.add(LocalDate.parse("1900-10-06"));
        bs_date.add(LocalDate.parse("1950-05-03"));
        bs_date.add(LocalDate.parse("2000-07-04"));
        bs_date.add(LocalDate.parse("2050-06-06"));

        System.out.println("\nArrayUtil.binSearch(bs_date, LocalDate.parse(\"2000-07-04\")): " + ArrayUtil.binSearch(bs_date, LocalDate.parse("2000-07-04")));
//      System.out.println(ArrayUtil.binSearch(bs_date, bs_date.get(2)));

        ArrayList<Integer> selsort_int = new ArrayList<>();
        selsort_int.add(5);
        selsort_int.add(7);
        selsort_int.add(3);
        selsort_int.add(2);
        selsort_int.add(6);
        selsort_int.add(1);
        selsort_int.add(8);
        selsort_int.add(4);

        selsort_int = ArrayUtil.selectionSort(selsort_int);
        System.out.println("\nArrayUtil.selectionSort(selsort_int): " + selsort_int);

        ArrayList<LocalDate> selsort_date = new ArrayList<>();
        selsort_date.add(LocalDate.parse("2000-07-04"));
        selsort_date.add(LocalDate.parse("0966-12-24"));
        selsort_date.add(LocalDate.parse("2002-07-04"));
        selsort_date.add(LocalDate.parse("1990-03-12"));

        selsort_date = ArrayUtil.selectionSort(selsort_date);
        System.out.println("\nArrayUtil.selectionSort(selsort_date): " + selsort_date);

        ArrayList<Integer> mergesort_int = new ArrayList<>();
        mergesort_int.add(5);
        mergesort_int.add(7);
        mergesort_int.add(3);
        mergesort_int.add(2);
        mergesort_int.add(6);
        mergesort_int.add(1);
        mergesort_int.add(8);
        mergesort_int.add(9);
        mergesort_int.add(4);

        mergesort_int = ArrayUtil.mergeSort(mergesort_int);
        System.out.println("\nArrayUtil.mergeSort(mergesort_int): " + mergesort_int);

        ArrayList<LocalDate> mergesort_date = new ArrayList<>();
        mergesort_date.add(LocalDate.parse("2000-07-04"));
        mergesort_date.add(LocalDate.parse("0966-12-24"));
        mergesort_date.add(LocalDate.parse("2002-07-04"));
        mergesort_date.add(LocalDate.parse("1990-03-12"));

        mergesort_date = ArrayUtil.mergeSort(mergesort_date);
        System.out.println("\nArrayUtil.mergeSort(mergesort_date): " + mergesort_date);

    }
}
