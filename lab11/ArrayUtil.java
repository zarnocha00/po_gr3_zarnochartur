package lab11;

import java.util.ArrayList;

public class ArrayUtil {

    public static <T extends Comparable<? super T>> boolean isSorted (ArrayList<T> array) {
        
        // dodatkowe zabezpieczenie
        if(Comparable.class.isAssignableFrom(array.getClass())) {
            return false;
        }


        ArrayList<T> unsorted_array = (ArrayList<T>) array.clone();
        array.sort(T::compareTo);

        System.out.println("unsorted array: " + unsorted_array);
        System.out.println("sorted array: " + array + "\n");

        for(int i = 0; i < array.size(); i++) {
            if (!(array.get(i).equals(unsorted_array.get(i)))) {
                return false;
            }
        }
        return true;

    }

    public static <T extends Comparable<? super T>> int binSearch (ArrayList<T> array, T obj) {
//        int mid = array.size()/2;
        int mid;
        int left = 0;
        int right = array.size()-1;

        if (!(array.contains(obj))) {
            return -1;
        }

        while (left < right) {
            mid  = (left+right)/2;

            if (obj.compareTo(array.get(mid)) == 0) {
                return mid;
            }

            if (obj.compareTo(array.get(mid)) >= 1) {
                left = mid;
            }
            else if ((obj.compareTo(array.get(mid)) <= -1)) {
                right = mid;
            }

            if (left == mid && left+1 == right) {
                return mid + 1;
            }
//            array = (ArrayList<T>) ;
//            array = new ArrayList<T>(array.subList(left, right));
        }

            return -1;
    }

    public static <T extends Comparable<? super T>> ArrayList<T> selectionSort (ArrayList<T> array) {
        int min_index = 0;
        for(int i = 1; i < array.size()-1; i++) {
            if(array.get(min_index).compareTo(array.get(i)) >= 1) {
                min_index = i;
            }
        }

        T tmp = array.get(0);
        array.set(0, array.get(min_index));
        array.set(min_index, tmp);

        if(array.size() > 2) {
            for(int i = 1; i < array.size(); i++) {
                min_index = i;
                for(int j = i+1; j <= array.size()-1; j++) {
                    if(array.get(min_index).compareTo(array.get(j)) >= 1) {
                        min_index = j;
                    }
                }
                tmp = array.get(i);
                array.set(i, array.get(min_index));
                array.set(min_index, tmp);
            }
        }
        return array;
    }

    public static <T extends Comparable<? super T>> ArrayList<T> mergeSort (ArrayList<T> array) {
        int left = 0;
        int right = array.size();
        int mid = (array.size()/2);

        ArrayList<T> left_tab = new ArrayList<>(array.subList(left, mid));
        ArrayList<T> right_tab = new ArrayList<>(array.subList(mid, right));

        if (left_tab.size() > 1) left_tab = mergeSort(left_tab);
        if (right_tab.size() > 1) right_tab = mergeSort(right_tab);

        if(left_tab.size() == 1 || right_tab.size() == 1) {
            if (left_tab.get(0).compareTo(right_tab.get(0)) >= 1) {
                ArrayList<T> merge_tab = new ArrayList<>(right_tab);
                merge_tab.addAll(left_tab);
                return merge_tab;
            }
        }
        else {
            ArrayList<T> merge_tab = new ArrayList<>(left_tab);
            merge_tab.addAll(right_tab);
            // wydaje mi sie, ze przez to selectionSort tutaj ten kod staje sie bezuzyteczny - rownie dobrze mozna byloby uzyc samego sel. sort, bez uzywania tego merge
            // mimo wszystko, mniejsze tablice sa sortowane, wiec moze jakis sens jest? nie wiem, nie jestem w stanie ocenic. inaczej nie umialem tego kodu napisac, mysle, ze musialbym jeszcze troche nad tym posiedziec, zeby zrobic to dobrze:)
            selectionSort(merge_tab);
            return merge_tab;
        }
    return array;
    }
}
