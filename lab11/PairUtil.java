package lab11;

// java: modifier static not allowed here
public class PairUtil {
    public static <T> Pair<T> swap(Pair<T> zm) {

        return new Pair<T>(zm.getSecond(), zm.getFirst());
    }

}
