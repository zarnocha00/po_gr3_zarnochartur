package lab12;

import java.util.LinkedList;

public class redukowanie {


    public static void redukuj(LinkedList<String> pracownicy, int n) {

        for (int i = 0; i < pracownicy.size(); i++) {

            if(i > 0 && i+1 <= pracownicy.size() && (i+1) % n == 0) {
                pracownicy.set(i, "remove!");
            }

        }

        if(n == 1) {
                pracownicy.set(0, "remove!");
        }

        // w petli uzywalem pracownicy.remove(i), ale indeksy sie automatycznie przesuwaly wiec musialem cos podstawic, zeby lista sie nie "poruszala"
        // chcialem uzyc 'null' zeby zaoszczedzic pamiec, ale do metody removeAll potrzebowalem tego samego typu, w ktorym jest dana lista

        LinkedList<String> removal = new LinkedList<>();
        removal.add("remove!");
        pracownicy.removeAll(removal);
    }

    public static <T> void generyczne_redukuj(LinkedList<T> pracownicy, int n) {

        for (int i = 0; i < pracownicy.size(); i++) {

            if(i > 0 && i+1 <= pracownicy.size() && (i+1) % n == 0) {
                pracownicy.set(i, null);
            }

        }

        if(n == 1) {
            pracownicy.set(0, null);
        }

        LinkedList<T> removal = new LinkedList<>();
        removal.add(null);
        pracownicy.removeAll(removal);
    }

}
