package lab12;

import java.util.LinkedList;

public class redukowanie_main {
    public static void main(String[] args) {

        LinkedList<String> pracownicy = new LinkedList<>();
        pracownicy.add("1. Tester");
        pracownicy.add("2. Hacker");
        pracownicy.add("3. Cracker");
        pracownicy.add("4. Methody");
        pracownicy.add("5. Player");

        LinkedList<String> ci_sami_pracownicy = (LinkedList<String>) pracownicy.clone();

        System.out.println("Przed redukcją: " + pracownicy);
        redukowanie.redukuj(pracownicy, 2);
        System.out.println("Po redukcji: " + pracownicy);

        System.out.println("\nPrzed redukcją: " + ci_sami_pracownicy);
        redukowanie.generyczne_redukuj(ci_sami_pracownicy, 3);
        System.out.println("Po redukcji: " + ci_sami_pracownicy);
    }
}
