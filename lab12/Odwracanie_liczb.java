package lab12;

import java.util.Stack;

public class Odwracanie_liczb {

    public static Stack<Integer> odwracanie_numeru(int liczba) {
        Stack<Integer> numer = new Stack<>();

        while (liczba != 0) {
            numer.push(liczba % 10);
            liczba = liczba/10;
        }

        return numer;
    }

    public static void main(String[] args) {
        int a = 2015;
        Stack<Integer> aa = odwracanie_numeru(a);
        while (!aa.empty()) {
            System.out.print(aa.pop() + " ");
        }
        System.out.println();

        int b = 2022;
        Stack<Integer> bb = odwracanie_numeru(b);
        while (!bb.empty()) {
            System.out.print(bb.pop() + " ");
        }

    }
}