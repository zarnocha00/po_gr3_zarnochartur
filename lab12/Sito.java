package lab12;

import java.util.ArrayList;

public class Sito {

    public static void sito_Eratostenesa(int n) {

        ArrayList<Integer> primes = new ArrayList<>();

        for (int i = 2; i <= n; i++) {
            primes.add(i);
        }

        for (int i = 2; i <= Math.sqrt(n); i++) {
            for (int j = 0; j < primes.size(); j++) {
                if (primes.get(j) % i == 0 && primes.get(j) != i) {
                    primes.remove(j);
                }
            }
        }

        for (int i = 0; i < primes.size()-1; i++) {
            System.out.print(primes.get(i) + ", ");
        }
        System.out.print(primes.get(primes.size()-1));

    }

    public static void main(String[] args) {
        sito_Eratostenesa(100);
    }
}
