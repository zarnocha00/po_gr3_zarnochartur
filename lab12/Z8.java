package lab12;

import java.util.*;

public class Z8 {
    public static  <T extends Iterable<?>>  void print(T obj) {
        for (Object arg: obj) {
            System.out.print(arg + ", ");
        }

    }

    public static void main(String[] args) {

        ArrayList<Integer> tab = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        print(tab);

        Stack<String> tab1 = new Stack<>();
        tab1.addAll(Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j"));
        System.out.println();
        print(tab1);

        List<Boolean> tab2 = new ArrayList<>(Arrays.asList(true, true, true, true, false, false, false, false));
        System.out.println();
        print(tab2);

        LinkedList<Double> tab3 = new LinkedList<>(Arrays.asList(0.12, 1.23, 2.34, 3.45, 4.56, 5.67, 6.78, 7.89));
        System.out.println();
        print(tab3);
    }

}
