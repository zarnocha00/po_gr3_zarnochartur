package lab12;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

public class odwracanie_slow {

    public static Stack<String> odwracanie(String zdanie) {

        Stack<String> stak = new Stack<>();
        ArrayList<String> zdanie_array = new ArrayList<>();
        Collections.addAll(zdanie_array, zdanie.split(" "));

        int poczatek_zdania = 0;

        for (int i = 0; i < zdanie_array.size(); i++) {
            String slowo = zdanie_array.get(i);
            int last_idx = slowo.length() - 1;

            char ostatni_znak = slowo.charAt(last_idx);

            if (ostatni_znak == '.') {
                zdanie_array.set(poczatek_zdania, ((String) (zdanie_array.get(poczatek_zdania).toLowerCase()) + "."));

                String slowo_bez_kropki = zdanie_array.get(i);
                slowo_bez_kropki = slowo_bez_kropki.substring(0, slowo_bez_kropki.length() - 1);
                char wielka_litera = Character.toUpperCase(slowo_bez_kropki.charAt(0));

                String slowo_bez_kropki_copy = wielka_litera + slowo_bez_kropki.substring(1, slowo_bez_kropki.length());
                zdanie_array.set(i, slowo_bez_kropki_copy);

                for (int j = i; j >= poczatek_zdania; j--) {
                    stak.add(zdanie_array.get(j));
                }
                poczatek_zdania = i + 1;
            }
        }

        Stack<String> tmp = (Stack<String>) stak.clone();
        stak.clear();
        for (int j = tmp.size()-1; j >= 0; j--) {
            stak.push(tmp.get(j));
        }

        return stak;
    }

    public static void main(String[] args) {

        String zdanie = "Ala ma kota. Jej kot lubi myszy.";
        Stack<String> zdanie_jako_stack = odwracanie(zdanie);
        while(!zdanie_jako_stack.empty()) {
            System.out.print(zdanie_jako_stack.pop() + " ");
        }
        System.out.println("\n");

        String kolejne_zdanie = "Abecadło z pieca spadło. O ziemię się hukło. Rozsypało się po kątach.";
        Stack<String> kolejne_zdanie_jako_stack = odwracanie(kolejne_zdanie);

        while(!kolejne_zdanie_jako_stack.empty()) {
            System.out.print(kolejne_zdanie_jako_stack.pop() + " ");
        }

    }

}
