package lab12;

import java.util.LinkedList;

public class odwracanie {
    public static void odwroc(LinkedList<String> lista) {

        int size = lista.size();
        LinkedList<String> tmp = (LinkedList<String>) lista.clone();

        lista.clear();

        for (int i = size-1; i >= 0; i--) {
            lista.add(tmp.get(i));
        }

    }

    public static <T> void generyczne_odwroc(LinkedList<T> lista) {

        int size = lista.size();
        LinkedList<T> tmp = (LinkedList<T>) lista.clone();

        lista.clear();

        for (int i = size-1; i >= 0; i--) {
            lista.add(tmp.get(i));
        }

    }
}
