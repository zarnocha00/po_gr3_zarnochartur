package lab12;

import java.util.LinkedList;

public class odwracanie_main {
    public static void main(String[] args) {
        LinkedList<String> lista = new LinkedList<>();
        lista.add("0");
        lista.add("1");
        lista.add("2");
        lista.add("3");
        lista.add("4");
        lista.add("5");

        System.out.println("Przed odwracaniem: " + lista);
        odwracanie.odwroc(lista);
        System.out.println("Po odwracaniu: " + lista);

        LinkedList<Integer> lista_int = new LinkedList<>();
        lista_int.add(5);
        lista_int.add(4);
        lista_int.add(3);
        lista_int.add(2);
        lista_int.add(1);
        lista_int.add(0);

        System.out.println("\nPrzed odwracaniem: " + lista_int);
        odwracanie.generyczne_odwroc(lista_int);
        System.out.println("Po odwracaniu: " + lista_int);

    }
}
