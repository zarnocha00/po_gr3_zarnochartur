package lab6;

public class IntegerSet {
    private boolean[] tablica;

    public static IntegerSet union(IntegerSet a, IntegerSet b) {
        IntegerSet c = new IntegerSet();
        for(int i = 0; i < a.tablica.length; i++) {
            if (a.tablica[i] || b.tablica[i]) {
                c.tablica[i] = true;
            }
        }
        return c;
    }
    public static IntegerSet intersection(IntegerSet a, IntegerSet b) {
        IntegerSet c = new IntegerSet();
        for(int i = 0; i < a.tablica.length; i++) {
            if(a.tablica[i] && b.tablica[i]) {
                c.tablica[i] = true;
            }
        }
        return c;
    }

    public IntegerSet() {
        tablica = new boolean[100];
    }

    void insertElement(int liczba) {
        this.tablica[liczba] = true;
    }

    void deleteElement(int liczba) {
        this.tablica[liczba] = false;
    }

    public String toString() {
        String str = "";
        for (int i = 0; i < 100; i++) {
            str += tablica[i];
            str += " ";
        }
        return str;
    }

    public String toString2() {
        String str = "";
        for (int i = 0; i < 100; i++) {
            if (tablica[i]) {
                if(i == 0) {
                    str = i + ". " + tablica[i];
                }
                else {
                    str = str + " " + i + ". " + tablica[i];
                }
            }
        }
        return str;
    }

    public boolean equals(IntegerSet a) {
        for(int i = 0; i < a.tablica.length; i++) {
            if(this.tablica[i] != a.tablica[i]) {
                return false;
            }
        }
        return true;
    }
}
