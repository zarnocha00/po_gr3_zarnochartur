package lab6;

import java.math.BigDecimal;

public class RachunekBankowy {
    static double rocznaStopaProcentowa;
    private BigDecimal saldo;

    public BigDecimal obliczMiesieczneOdsetki() {
        BigDecimal odsetki = (saldo.multiply(new BigDecimal(rocznaStopaProcentowa))).divide(new BigDecimal(12), 2, BigDecimal.ROUND_CEILING);
        saldo = saldo.add(odsetki);
        return saldo;
    }

    public static void setRocznaStopaProcentowa(double nowaStopaProcentowa) {
        rocznaStopaProcentowa = nowaStopaProcentowa;
    }

    public RachunekBankowy(BigDecimal inicjalizacja) {
        saldo = inicjalizacja;
    }

    public void wypiszSaldo() {
        System.out.println("Saldo wynosi " + saldo);
    }
}