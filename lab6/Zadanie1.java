package lab6;

import java.math.BigDecimal;

public class Zadanie1 {
    public static void main(String[] args) {
        RachunekBankowy ciulacz1 = new RachunekBankowy(new BigDecimal(2000.0));
        RachunekBankowy ciulacz2 = new RachunekBankowy(new BigDecimal(3000.0));

        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        ciulacz1.obliczMiesieczneOdsetki();
        ciulacz2.obliczMiesieczneOdsetki();

        System.out.println("ciulacz1:");
        ciulacz1.wypiszSaldo();
        System.out.println("\nciulacz2:");
        ciulacz2.wypiszSaldo();

        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        ciulacz1.obliczMiesieczneOdsetki();
        ciulacz2.obliczMiesieczneOdsetki();

        System.out.println("\nciulacz1:");
        ciulacz1.wypiszSaldo();
        System.out.println("\nciulacz2:");
        ciulacz2.wypiszSaldo();
    }
}
