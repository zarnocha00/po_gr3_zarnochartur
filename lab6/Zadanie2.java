package lab6;

public class Zadanie2 {
    public static void main(String[] args) {

        IntegerSet obj1 = new IntegerSet();
        obj1.insertElement(1);

        // przy printowaniu uzylem metody obj1.toString(), ale IDE pokazuje mi, ze niepotrzebnie tego uzywam, takze jezeli jest to blad to wina srodowiska
        // dla przejrzystszego odczytywania wartosci, pomijajac elementy bedace 'false' mozemy podmienic f-cje toString na toString2 w implementacji IntegerSet albo przy printowaniu
        // System.out.println("obj1.insertElement(1):\n" + obj1.toString2());
        System.out.println("obj1.insertElement(1):\n" + obj1);

        obj1.deleteElement(1);
        System.out.println("\nobj1.deleteElement(1):\n" + obj1);

        IntegerSet obj2 = new IntegerSet();
        obj2.insertElement(2);

        obj1.insertElement(1);
        System.out.println("\nobj1 & obj2 union:\n" + IntegerSet.union(obj1, obj2));
//        System.out.println("\nobj1 & obj2 union:\n" + IntegerSet.union(obj1, obj2).toString2());

        obj1.insertElement(2);
        System.out.println("\nobj1 & obj2 intersection:\n" + IntegerSet.intersection(obj1, obj2));

        System.out.println("\nobj1 & obj2 equals (false):\n" + obj2.equals(obj1));

        System.out.println("\nintersection(obj1, obj2) & obj2 equals (true):\n" + obj2.equals(IntegerSet.intersection(obj1, obj2)));
    }
}
